# sisop-praktikum-fp-2023-bs-d11
# Kelompok D11 - SISOP D 

NRP | NAMA
------------- | -------------
5025211196  | Sandyatama Fransisna Nugraha
5025211105  | Sarah Nurhasna Khairunnisa
5025211148  | Katarina Inezita Prambudi

<br>

# Penjelasan Kode Database 

## 1. Library

```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
```
Kode tersebut adalah header dan konstanta yang digunakan dalam sebuah program socket. Program socket adalah program yang menggunakan socket untuk mengirim dan menerima data melalui jaringan

<br>

## 2. Port & User

```c
// port
#define PORT 8080
#define MAX_USER 1000
```

<br>
Baris ini mendefinisikan konstanta PORT dengan nilai 8080 dan MAX_USER dengan nilai 1000. Konstanta ini digunakan untuk menentukan nomor port yang akan digunakan dalam koneksi socket dan batas maksimum jumlah pengguna.

<br>

## 3. isConnectionClosed

```c
bool isConnectionClosed(int readValue, int *newSocket){
    if(readValue == 0){ 
        printf("Socket closed\n");
        close(*newSocket); 
        return 1;
    }
    return 0;
}
```
Kode program yang Anda berikan adalah sebuah fungsi bernama isConnectionClosed yang digunakan dalam socket programming. Fungsi ini digunakan untuk memeriksa apakah koneksi socket telah ditutup oleh sisi lain.

<br>

## 4. Check User

```c
bool check_user(char *user){

    FILE *fileUser = fopen(userPath,"r");
    char usertmp[1000];
    char passtmp[1000];

    if(fileUser){
        while(fscanf(fileUser,"%s %s",usertmp,passtmp) != EOF){ 
            if(!strcmp(usertmp,user)){
                printf("ERROR: user not found\n");
                return 1; 
            }
        }
    }
    return 0;
}
```
Fungsi check_user digunakan untuk memeriksa keberadaan pengguna dalam file pengguna yang valid. Hal ini berguna dalam mengimplementasikan mekanisme otentikasi pengguna dalam pemrograman socket, sehingga hanya pengguna yang terdaftar dan valid yang diperbolehkan mengakses layanan atau sumber daya tertentu melalui koneksi socket.

Informasi user dan password user tersimpan dalam file `user.txt` yang secara otomatis akan dibuat ketika root menggunakan fungsi `use()`.

<br>

## 5. Check Database

```R
bool check_database(char *data_base){

    FILE *filedb = fopen(permissionPath,"r");
    char usertmp[1000];
    char databasetmp[1000];

    
    if(filedb){
        while(fscanf(filedb,"%s %s",databasetmp,usertmp) != EOF){ 
            if(!strcmp(databasetmp,data_base)){ 
                return 1; 
            }
        }
    }
    return 0;
}
```
Fungsi check_database digunakan untuk memeriksa keberadaan basis data dalam file izin. Hal ini berguna dalam pemrograman socket untuk mengatur akses basis data hanya kepada pengguna yang diizinkan.

Permission database diberikan oleh root dengan menggunakan fungsi `grant_permission()` yang secara otomatis juga akan membuat file `permission.txt` untuk menyimpan informasi user dan database yang boleh digunakan oleh user tersebut.

<br>

## 6. Generate Command

```c
void generate_command(int *newSocket, char *useDatabase){

    DIR *dp;
    struct dirent *entry;
    char dbpath[1000] = {0};
    sprintf(dbpath,"%s/databases/%s",databasePath,useDatabase); 

    dp = opendir(dbpath);

    if (dp != NULL){
        while ((entry = readdir (dp))) {
            if(!strncmp(entry->d_name,"struktur_",9)){ 

                char table_name[1000] = {0}; char temp[1000] = {0};
                strcpy(temp,entry->d_name);
                char *token = strtok(temp,"_");
                token = strtok(NULL,"_");

                strcpy(table_name,token);
                char kolom[100][1000] = {0}; 
                int jumlah_kolom = 0;
                char path[1000] = {0};

                
                sprintf(path,"%s/databases/%s/%s",databasePath,useDatabase,entry->d_name); 
               
                FILE *file_in = fopen(path,"r");
                if(file_in){
                    char temp2[1000];
                    while((fscanf(file_in,"%[^\n]%*c",temp2)) != EOF){
                        strcpy(kolom[jumlah_kolom++],temp2);
                    }

                   
                    fclose(file_in);

                    
                    int readValue;
                    char command[1000] = {0};
                    char buffer[1024] = {0};
                    char table[1000] = {0};

                  
                    strcpy(table,table_name);
                  
                    char *token_table = strtok(table,"."); 
                    
                    //CREATE TABLE
                    sprintf(command,"CREATE TABLE %s (",token_table); 
                    for(int i = 0; i < jumlah_kolom; i++){
                        strcat(command,kolom[i]); 
                        strcat(command,","); 
                    } 
                    command[strlen(command)-1] = ')';             
                    strcat(command,";"); 
                    send(*newSocket , command , strlen(command) , 0 );                    
                    readValue = recv( *newSocket , buffer, 1024, 0);
                    bzero(path,sizeof(path)); 


                    // DROP TABLE
                    sprintf(command,"DROP TABLE %s;",token_table); 
                    send(*newSocket , command , strlen(command) , 0 );
                    readValue = recv( *newSocket , buffer, 1024, 0);
                    bzero(command,sizeof(command)); 
                    sprintf(path,"%s/databases/%s/%s",databasePath,useDatabase,table_name); 

                    file_in = fopen(path,"r");
                    if(file_in){
                        while((fscanf(file_in,"%[^\n]%*c",temp2)) != EOF){
                            bzero(command,sizeof(command)); 

                            // INSERT INTO _ VALUES _
                            sprintf(command,"INSERT INTO %s VALUES (%s);",token_table,temp2); 
                            send(*newSocket , command , strlen(command) , 0 );
                            readValue = recv( *newSocket , buffer, 1024, 0);
                        }
                        fclose(file_in);
                    }
                }   
            }
        }


        // EXIT COMMAND
        char command[1000] = {0}; 
        bzero(command,sizeof(command)); 
        strcpy(command,"EXIT");
        send(*newSocket , command , strlen(command) , 0 );
        (void) closedir (dp);

    } else {
        printf ("ERROR: Couldn't open the directory");
    }
}
```
Fungsi generate_command digunakan dalam pemrograman socket untuk menghasilkan dan mengirimkan perintah-perintah SQL kepada koneksi socket baru, yang terkait dengan operasi pada tabel dalam basis data yang ditentukan.

<br>

## 7. Write

```R
void writes(char path[1000],char text[1000]){
    FILE* ptr = fopen(path,"a");
    fprintf(ptr,"%s\n",text);
    fclose(ptr);
}
```
Fungsi writes digunakan untuk menambahkan teks ke dalam file yang sudah ada dalam pemrograman socket. 

Fungsi ini akan digunakan untuk menulis informasi pada file `user.txt`, `permission.txt`, `struktur_tabel.txt`, dan `tabel1.txt`.

<br>

## 8. Save Log

```c
void save_log(char *login_user, char *command){
   
    char fpath[1000]={0};
    sprintf(fpath,"%s/dblog.log",databasePath); 

    FILE *file_out = fopen(fpath,"a");
    char time_now[100] = {0};
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	strftime(time_now,100,"%Y-%m-%d %H:%M:%S",tm); 
    fprintf(file_out, "%s:%s:%s\n",time_now,login_user,command); 
    fclose(file_out);
}
```
Fungsi save_log digunakan untuk menyimpan log aktivitas pengguna ke dalam file `dblog.txt` dalam pemrograman socket.

<br>

## 9. Trim

```R
char* trim(char *text){
    int index = 0;
    while(text[index] == '\t' || text[index] == ' ') index++;
    char *temp = strchr(text,text[index]);
    return temp;
}
```
Fungsi trim digunakan untuk memotong (trim) spasi atau tab di awal sebuah teks dalam pemrograman socket. 

Fungsi ini akan digunakan dan berfungsi di dalam fungsi `insert()` untuk menghapus spasi/tab yang diinputkan oleh user ketika melakukan command `INSERT INTO`.

<br>

## 10. Create User

```c
int create_user(char *buffer,char *type){

    char tempBuffer[1024] = {0}; 
    strcpy(tempBuffer,buffer);
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");

   
    int i=0;
    char input[10][1000];
    
   
    if(strcmp(type,"root")){ 
        printf("ERROR: Permission denied\n");
        return 0; 
    }

   
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }


    if(i < 6){
        printf("ERROR: syntax error\n");
        return -1;
    }
   
    if(strcmp("IDENTIFIED",input[3]) || strcmp("BY",input[4]) || buffer[strlen(buffer)-1] != ';'){
        printf("ERROR: syntax error\n");
        return -1;
    }

   
    if(strcmp(input[2],"root") != 0 && !check_user(input[2])){ 
        char tmp[1000];
        strcpy(tmp,input[2]);
        strcat(tmp," ");
        strcat(tmp,input[5]);
        writes(userPath,tmp);
        return 1;
    }else {
        printf("ERROR: create user failed\n");
        return -2;
    }
}
```
Fungsi create_user digunakan untuk membuat pengguna baru dengan izin yang ditentukan dalam pemrograman socket.

Fungsi ini digunakan untuk menjalankan command `CREATE USER` yang hanya bisa dilakukan oleh root.

<br>

## 11. Use Database

```c
int use(char *buffer, char *type, char *login_user, char *useDatabase){
    
    char tempBuffer[1024] = {0};
    char database[100] = {0};
    strcpy(tempBuffer,buffer);
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    

    if(token != NULL){
        strcpy(database,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    FILE *file_in = fopen(permissionPath,"r");

    bool ada = false;
    char databasetmp[1000];
    char usertmp[1000];
    
  
    if(file_in){
        while(fscanf(file_in,"%s %s",databasetmp,usertmp) != EOF){
            if(!strcmp(databasetmp,database)){
                ada = true;
                if(!strncmp(type,"root",4) || !strcmp(usertmp,login_user)){
                    fclose(file_in);
                    strcpy(useDatabase, database);
                    return 1;
                }
            }
        }
    }
    if(ada == false){
        printf("ERROR: syntax error\n");
        return -2;
    }
    return 0;
}
```
Fungsi use digunakan untuk mengatur penggunaan basis data (database) dalam pemrograman socket.

Fungsi use digunakan untuk menjalankan command `USE` yang berfungsi untuk menggunakan database yang memiliki permission. Oleh karena itu, perlu dilakukan pengecekan permission database pada file `permission.txt`.

Fungsi use juga menentukan untuk jalannya command CRUD, karena apabila database belum digunakan, maka user tidak dapat melakukan CRUD pada database tersebut.

<br>

## 12. Create Database 

```c
int create_database(char *buffer, char *type, char *login_user){
  
    char tempBuffer[1024] = {0};
    char data_base[100] = {0};
    strcpy(tempBuffer,buffer);
    
   
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    
    if(token != NULL){
        strcpy(data_base,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

  
    char fpath[1000] = {0};
    sprintf(fpath,"%s/databases/%s",databasePath,data_base); 
    

    if(mkdir(fpath,0777) == 0){
        char record[1000] = {0};
        sprintf(record,"%s %s", data_base, login_user);
        writes(permissionPath,record);      
        return 1;
    }else{
        return 0;
    }
}

```
Fungsi create_database digunakan dalam pemrograman socket untuk membuat basis data (database) baru.

Fungsi create_database digunakan untuk menjalankan command `CREATE DATABASE`.

Fungsi create_database akan membuat folder `database` yang di dalamnya terdapat file `struktur_tabel.txt` yang berisi struktur tabel dan `tabel1.txt` yang akan berisi value dari tabel setelah dilakukan command `INSERT INTO`.

<br>

## 13. Grant Permission

```c
int grant_pemission(char *buffer, char *type){
    
    int i=0;
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
   
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    char input[10][1000];
    
    if(strcmp(type,"root")){
        printf("ERROR: Permission denied\n");
        return 0;
    }

    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    if(i < 5){
        printf("ERROR: syntax error\n");
        return -1;
    }
    
    if(buffer[strlen(buffer)-1] != ';' || strcmp("INTO",input[3])){
        printf("ERROR: Syntax error\n");
        return -1;
    }

    char tmp[1000] = {0};
    if(check_database(input[2])){
        if(check_user(input[4])){         
            sprintf(tmp, "%s %s", input[2], input[4]);
            writes(permissionPath,tmp);
            return 1;
        }else{
            return -3;
        }
    }else{
        return -2;
    }
}
```
Fungsi grant_permission digunakan dalam pemrograman socket untuk memberikan izin akses pengguna ke suatu basis data (database).

Fungsi grant_permission digunakan untuk menjalankan command `GRANT PERMISSION` yang hanya dapat dilakukan oleh root.

<br>

## 14. Create Table

```c
int create_table(char *buffer,char *useDatabase){

    if(strlen(useDatabase) == 0){
        printf("ERROR: database not found\n");
        return -2;
    }

    char tempBuffer[1024] = {0};
    char table[100] = {0};
    
    strcpy(tempBuffer,buffer);
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    if(token != NULL){
        strcpy(table,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    char *temp;
    temp = strchr(buffer,'(');
    if(!temp)return -1;
    else temp = temp + 1;
    
    char kolom[1000] = {0};
    strcpy(kolom,temp);

    char *token1 = strtok(kolom,";");
    token1 = strtok(kolom,")");
    int ind = 0;
    char split_kolom[100][100] = {0};
    token1 = strtok(kolom,",");
    while(token1!=NULL){
        strcpy(split_kolom[ind++],trim(token1)); 
        token1 = strtok(NULL,",");
    }

    for(int i = 0; i < ind; i++){
        char temp2[1000] = {0};
        char nama_kolom[100] = {0};
        char type_column[20] = {0};

        strcpy(temp2,split_kolom[i]); 
        char *token2 = strtok(temp2," ");

        strcpy(nama_kolom,token2);
        token2 = strtok(NULL," ");
        if(token2){
            strcpy(type_column,token2);
            if(strcmp(type_column,"int") && strcmp(type_column,"string")){ 
                return -4;
            }
        }else{
            return -3;
        }
    }

    char fpath[1000] = {0};
    sprintf(fpath,"%s/databases/%s/%s.txt",databasePath,useDatabase,table);
    FILE *open;
    if(!(open = fopen(fpath,"r"))){
        open = fopen(fpath,"w");
        char struktur_table[1000] = {0};
        sprintf(struktur_table,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,table);

        for(int i=0; i<ind; i++){
            writes(struktur_table,split_kolom[i]);
        }
        return 1;
    }else{
        printf("ERROR: syntax error\n");
        return 0;
    }
}
```
Fungsi create_table digunakan dalam pemrograman socket untuk membuat tabel baru dalam suatu basis data (database).

Fungsi create_table digunakan untuk menjalankan command `CREATE TABLE`.

<br>

## 15. Delete Direcctory
```c
void *delete_directory(void *arg){
    // inisialisasi fork
    pid_t child = fork();
    if(child < 0){
        exit(EXIT_FAILURE);
    }
    // child process
    else if(child == 0){
        char *fpath = (char *) arg;
        // gunakan command rm untuk meremove directory
        char *argv[] = {"rm","-rf",fpath, NULL}; //gunakan -rf untuk menghapus directory beserta file di dalamnya
        execv("/bin/rm",argv); // eksekusi argv
    }
}
```
Fungsi delete_directory digunakan untuk menghapus direktori dan seluruh isinya.

Fungsi ini akan digunakan dalam fungsi `drop_database()`.

<br>

## 16. Drop Database 

```c
int drop_database(char *buffer, char *type, char *login_user, char *useDatabase){

    char tempBuffer[1024] = {0};
    char data_base[100] = {0};
    strcpy(tempBuffer,buffer);
    
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");
    
    if(token != NULL){
        strcpy(data_base,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    FILE *file_in, *file_out;

    file_in = fopen(permissionPath,"r");

    char databasetmp[1000];
    char usertmp[1000];
    bool bisa = false;
    bool ada = false;

    if(file_in){
        while(fscanf(file_in,"%s %s",databasetmp,usertmp) != EOF){
            if(!strcmp(databasetmp,data_base)){
                ada = true;
                if(!strcmp(usertmp,login_user)){
                    fclose(file_in);
                    bisa = true;
                    break;
                }
            }
        }

        if((ada && !strcmp(login_user,"root")) || bisa){
         
            char fpath[1000] = {0};
            sprintf(fpath,"%s/databases/%s",databasePath,data_base); 
            pthread_t thread1;
            int iret1 = pthread_create(&thread1,NULL,delete_directory,fpath);;
            pthread_join(thread1,NULL);
            
            file_in = fopen(permissionPath,"r");
            char temp_permission[1000] = {0};

            sprintf(temp_permission,"%s/databases/administrator/temp.txt",databasePath); // --> databasePath/databases/administrator/temp.txt
            
            // open file temp_permission yaitu file temp.txt
            file_out = fopen(temp_permission,"w");
            // baca isi file sampai end of file
            while(fscanf(file_in,"%s %s",databasetmp,usertmp) != EOF){
                if(strcmp(databasetmp,data_base)){
                    char record[1000] = {0};
                    fprintf(file_out, "%s %s\n", databasetmp, usertmp);
                }
            }

            // close file temp.txt
            fclose(file_out);

            // remove
            remove(permissionPath);
            rename(temp_permission,permissionPath);

            // reset useDatabase
            if(!strcmp(useDatabase,data_base)){
                bzero(useDatabase,sizeof(useDatabase));
            }
            return 1;
        }        
    }
    if(!ada){
        printf("ERROR: Database not found\n");
        return -2;
    }
    return 0;
}
```
Fungsi drop_database digunakan untuk menghapus database beserta seluruh tabel dan isinya. Jika pengguna memiliki izin yang cukup dan database ditemukan, direktori database akan dihapus secara rekursif menggunakan thread. Setelah itu, catatan izin database akan diperbarui dan jika database yang dihapus sedang digunakan, pengaturan useDatabase akan direset.

Fungsi drop_database akan menghapus folder `database` beserta seluruh isinya.

Fungsi drop_database akan digunakan untuk menjalankan command `DROP DATABASE`

<br>

## 17. Validation

```C
int validasi(char *buffer){

    // inisialisasi
    char tmp[1000];
    strcpy(tmp,buffer);
    
    // pengecekan tanda petik
    char g = '\'';
    if(tmp[0]== g && tmp[strlen(tmp)-1] == g){
        return 1;
    }

    // pengecekan angka
    for(int i=0; i<strlen(tmp) ;i++){
        if(tmp[i] < '0' || tmp[i] > '9'){
            return 0;
        }
    }

    return 2;
}
```
Fungsi validasi digunakan untuk memvalidasi sebuah string dalam buffer. Fungsi ini melakukan pengecekan terhadap string untuk memastikan bahwa string tersebut memiliki format yang sesuai. Jika string diapit oleh tanda kutip satu (''), fungsi mengembalikan nilai 1. Jika string hanya terdiri dari angka, fungsi mengembalikan nilai 2. Jika string tidak memenuhi salah satu dari kedua kriteria tersebut, fungsi mengembalikan nilai 0.

Fungsi validasi digunakan untuk mengecek input value yang dilakukan user ketikan menjalankan command `INSERT INTO`.

<br>

## 18. Insert Command

```c
int insert(char *buffer, char *useDatabase){
    // jika database tidak ada
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }    

    // inisialisasi
    char tmp[1000];
    strcpy(tmp,buffer);

    // pisahkan token
    char *token = strtok(tmp,"(");
    token = strtok(NULL,"(");
    token = strtok(token,")");
    token = strtok(token,",");

    // inisialisasi
    int i = 0 , k = 0;
    char data[100][1000];

    // ketika token tidak null
    while(token!=NULL){
        strcpy(data[i],trim(token)); i++;
        token = strtok(NULL,",");
    }

    strcpy(tmp,buffer);
    token = strtok(tmp," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    // inisialisasi
    FILE *file_in,*file_out;
    char open[1000] = {0};
    char append[1000] = {0};

    // path
    sprintf(open,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,token);

    // open file open
    file_in = fopen(open,"r");
    // inisialisasi
    char data_type[100][1000];
    char tmp_type[1000],ret[1000];

    // Jika file ada
    if(file_in){
        // baca isi file sampai end of file
        while(fscanf(file_in,"%s %s",ret,tmp_type) != EOF){
            // copy data
            strcpy(data_type[k],tmp_type);
            k++;
        }
    }
    else{
        // printf("ERROR: File not found\n");
        return -2;
    }

    // path
    sprintf(append,"%s/databases/%s/%s.txt",databasePath,useDatabase,token);

    // open file append
    file_out = fopen(append,"a");
    if(k != i){
        // tutup file open
        fclose(file_in);
        // tutup file append
        fclose(file_out);
        return -3;
    }

    // validasi tipe data
    for(int j=0;j<i;j++){
        int val = validasi(data[j]);
        if(val == 1 && !strcmp(data_type[j],"string")) ; // jika tipe data string
        else if (val == 2 && !strcmp(data_type[j],"int")) ; // jika tipe data int
        else {
            // tutup file open
            fclose(file_in);
            // tutup file append
            fclose(file_out);
            printf("ERROR: syntax error\n");
            return -4;
        }  
    }

    // print data ke file
    for(int j=0;j<i-1;j++){
        fprintf(file_out,"%s,",data[j]);
    }
    fprintf(file_out,"%s\n",data[i-1]);
    // tutup file open
    fclose(file_in);
    // tutup file append
    fclose(file_out);

    return 1;
}
```
Fungsi insert digunakan untuk memasukkan data ke dalam tabel dalam database yang sedang digunakan. Fungsi ini memeriksa validitas tipe data, membuka file struktur tabel, dan memasukkan data ke dalam file tabel jika semuanya valid.

Fungsi insert digunakan untuk menjalankan command `INSERT INTO`.

<br>

## 19. Drop Table

```c
int drop_table(char *buffer, char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -2;
    }

    // inisialisasi
    char tmp[1000];
    char *token;
    // pisah token
    strcpy(tmp,buffer);
    token = strtok(tmp,";"); // pemisah tanda ";"
    token = strtok(token," "); // pemisah " "
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    char open[1000] = {0};
    // path
    sprintf(open,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,token);

    // buka file open
    FILE *file_in = fopen(open,"r");

    char append[1000]={0};
    // path
    sprintf(append,"%s/databases/%s/%s.txt",databasePath,useDatabase,token);

    // Jika file open ada
    if(file_in){
        // tutup file open
        fclose(file_in);

        // remove file
        remove(open);
        remove(append);
        return 1;
    }
    else{
        printf("ERROR: syntax error\n");
        return -1;
    }
}
```
Fungsi drop_table digunakan untuk menghapus tabel dari database yang sedang digunakan. Fungsi ini membuka file struktur tabel, dan jika file tersebut ada, maka file struktur dan file tabel akan dihapus dari database. Jika file tidak ditemukan, akan ditampilkan pesan error.

Fungsi drop_table digunakan untuk menjalankan command `DROP TABLE`.

<br>

## 20. Drop Column

```c
int drop_column(char *buffer, char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -2;
    }

    // inisialisasi
    int i = 0,j = 0,k = 0;
    char tmp[10000];
    char input[10][1000];
    
    strcpy(tmp,buffer);
    // pisahkan token
    char *token;
    token = strtok(tmp,";"); // pemisah tanda ";"
    token = strtok(token," "); // pemisah " "
    // perulangan selama token tidak null
    while (token != NULL) {
        strcpy(input[k++],token);
        token = strtok(NULL, " ");
    }

    if(k != 5){
        printf("ERROR: syntax error\n");
        return -3;
    }

    // inisialisasi
    FILE *strukturIn,*strukturOut,*tableIn,*tableOut;
    
    // inisialisasi
    char open[1000]={0};
    char append[1000]={0};
    char a[1000]={0};
    char b[1000]={0};

    // path
    sprintf(open,"%s/databases/%s/struktur_%s",databasePath,useDatabase,input[4]);
    strcpy(a,open); //copy
    strcat(a,"2.txt");
    strcat(open,".txt");

    // open file open
    strukturIn = fopen(open,"r");
   
    char data_type[1000];
    char name[1000];

    // selama file open ada
    if(strukturIn){
        strukturOut = fopen(a,"w");
        // baca isi file sampai end of file
        while(fscanf(strukturIn,"%s %s",name,data_type) != EOF){
            // compare input[2] dengan name
            if(strcmp(input[2],name)){
                fprintf(strukturOut,"%s %s\n",name,data_type);
            }
            else i = j;
            j++;
        }
        // tutup file open
        fclose(strukturIn);
        // tutup file append
        fclose(strukturOut);
    }
    else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    // path append
    sprintf(append,"%s/databases/%s/%s",databasePath,useDatabase,input[4]);
    strcpy(b,append); // copy append ke b
    strcat(b,"2.txt"); // print 2.txt di akhri file b
    strcat(append,".txt"); // print txt di akhri file b

    // open file append
    tableIn = fopen(append,"r");

    // oppen file b
    tableOut = fopen(b,"w");

    char ambil[1000];
    while(fgets(ambil,1000,tableIn)){
        // inisialisasi
        token = strtok(ambil,","); // pemisah ","
        int j = 0;
        char baru[1000];

        // copy "" ke baru
        strcpy(baru,"");

        // perulangan selama token tidak null
        while(token!=NULL){
            if(j!=i){
                strcat(baru,token); // print token di akhir file baru
                strcat(baru,","); // print "," di akhir file baru
            }
            token = strtok(NULL,",");
            j++;
        }
        baru[strlen(baru)-1] = 0;
        // print baru ke file b
        fprintf(tableOut,"%s",baru);
        if(baru[strlen(baru)-1] != '\n'){
            fprintf(tableOut,"\n");
        }
    }
    // close file open
    fclose(tableIn);
    // close file b
    fclose(tableOut);

    // remove file
    remove(open); rename(a,open);
    remove(append); rename(b,append);
    return 1;
}
```
Fungsi drop_column digunakan untuk menghapus kolom dari sebuah tabel dalam database yang sedang digunakan. Fungsi ini membaca file struktur tabel, menghapus kolom yang sesuai dengan input, dan kemudian mengupdate file tabel dengan menghapus nilai-nilai yang terkait dengan kolom tersebut. Jika file struktur atau file tabel tidak ditemukan, akan ditampilkan pesan error.

Fungsi drop_column digunakan untuk menjalankan command `DROP COLUMN`.

<br>

## 21. Login

```c
int login(char* type, char *login_user){
    // Open file userPath
    FILE *file_in = fopen(userPath,"r");
    printf("%s\n",type);

    // inisialisasi
    char user[1000],pass[1000];
    char usertmp[1000],passtmp[1000];
    // pisah token
    char* token = strtok(type, " ");
    strcpy(user,token);
    token = strtok(NULL, " ");
    strcpy(pass,token);
    
    // selama file tidak kosong
    if(file_in){
        // baca isi file sampai end of file
        while(fscanf(file_in,"%s %s",usertmp,passtmp) != EOF){
            // pengecekan permission dan user
            if(!strcmp(passtmp,pass) && !strcmp(usertmp,user)){
                // tutup file
                fclose(file_in);
                strcpy(login_user,user);
                return 1;
            }
        }
        // tutup file
        fclose(file_in);
    }
    return 0; 
}
```
Fungsi login digunakan untuk melakukan proses autentikasi pengguna. Fungsi ini membaca file yang berisi informasi pengguna dan membandingkan username dan password yang dimasukkan dengan entri yang ada dalam file. Jika ada kecocokan, pengguna dianggap berhasil login dan fungsi mengembalikan nilai 1. Jika tidak ada kecocokan atau file tidak dapat dibuka, fungsi mengembalikan nilai 0.

<br>

## 22. Delete Command (delete_from)

```c
int delete_from(char *buffer,char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }

    // inisialisasi buffer
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
    
    // pisah token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");

    // inisialisasi
    int i=0;
    char input[10][1000];

    // perulangan ketika token tidak null
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    if(i == 3){
        // path
        char path[10000];
        sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[2]);
        FILE *file_in,*file_out;

        // open file pada path
        file_in = fopen(path,"r");
        if(file_in){
            // tutup file
            fclose(file_in);

            // open file path
            file_out = fopen(path,"w");
            fprintf(file_out,"");

            // tutup file path
            fclose(file_out);
        }
        else{
            printf("ERROR: file not found\n");
            return -3;
        }
        return 1;
    }
    else if(i == 5){
        // Command Where
        if(strcmp(input[3],"WHERE")){
            return -2;
        }

        // inisialisasi
        char table_name[1000],table[1000];
        char path[10000],n_path[10000],struk[10000];
        char ret[1000], cmp[1000];
        int i = -1, k = 0;

        // pisah token
        token = strtok(input[4],"="); // tanda pemisah "="
        strcpy(table_name,token);

        token = strtok(NULL,"="); // tanda pemisah "="
        strcpy(cmp,token);

        sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[2]);

        // open file struk
        FILE *strukturIn = fopen(struk,"r");
        // Jika file ada
        if(strukturIn){
            // baca isi file sampai end of file
            while(fscanf(strukturIn,"%s %s",table,ret) != EOF){
                if(!strcmp(table_name,table)) i = k;
                k++;
            }
            if(i == -1){
                // tutup file
                fclose(strukturIn);
                return -4;
            }
            // tutup file
            fclose(strukturIn);
        }
        else{
            printf("ERROR: file not found\n");
            return -3;
        }

        // path
        sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[2]);
        sprintf(n_path,"%s/databases/%s/%s2.txt",databasePath,useDatabase,input[2]);

        // open file path
        FILE *file_in = fopen(path,"r");
        // open file n_path
        FILE *file_out = fopen(n_path,"w");
        char ambil[1000];

        // jika file path tersedia
        if(file_in){
            // ambil isi file
            while(fgets(ambil,1000,file_in)){
                token = strtok(ambil,","); // pemisah ","
                // inisialisasi
                int j = 0, flag = 0; 
                char baru[1000];

                strcpy(baru,"");
                // perulangan selama token tidak null
                while(token!=NULL){
                    strcat(baru,token); // print token di akhir file baru
                    strcat(baru,","); // print "," di akhir file baru
                    if(j==i){
                        // compare token dan cmp
                        if(!strcmp(cmp,token)) flag = 1;
                    }
                    token = strtok(NULL,",");
                    j++;
                }
                if(!flag){
                    baru[strlen(baru)-1] = 0;
                    // print baru ke dalam file
                    fprintf(file_out,"%s",baru);
                    if(baru[strlen(baru)-1] != '\n') fprintf(file_out,"\n");
                }
            }
            // tutup file path
            fclose(file_in);
            // tutup file n_path
            fclose(file_out);

            // remove file
            remove(path);
            rename(n_path,path);
            return 1;
        }
        else{
            printf("ERROR: file not found\n");
            return -3;
        }
    }
    else{
        return -2;
    }
}
```
Fungsi delete_from digunakan untuk menghapus data dari tabel dalam database. Fungsi ini menerima dua argumen, yaitu buffer yang berisi perintah SQL DELETE dan useDatabase yang menentukan database yang digunakan. Fungsi ini memproses perintah DELETE berdasarkan kondisi yang diberikan. Jika perintah berhasil dieksekusi, fungsi mengembalikan nilai 1. Jika terjadi kesalahan dalam sintaks atau file tidak ditemukan, fungsi mengembalikan nilai negatif (-1, -2, -3, atau -4) yang menunjukkan jenis kesalahan yang terjadi.

Digunakan untuk menjalankan command `DELETE FROM`.

<br>

## 23. Update Command

```c
int update(char *buffer,char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }

    // inisialisasi buffer
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
    
    // pisah token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");

    // inisialisasi
    int i=0 , k=0;
    char input[10][1000], table_name[1000], cmp[1000];
    char path[10000], n_path[10000], struk[10000];
    char ret[1000], table[1000], banding[1000],temp[1000],cmpwhere[1000];

    // perulangan ketika token tidak null
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    // Cek jika iput[2] bukan SET
    if(strcmp(input[2],"SET")){
        printf("ERROR: syntax error\n");
        return -2;
    }

    token = strtok(input[3],"="); // ambil token
    strcpy(table_name,token); // copy token
    token = strtok(NULL,"="); // ambil token
    strcpy(cmp,token); // copy token

    sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[1]);

    // open file struk
    FILE *strukturIn = fopen(struk,"r");
    i = -1;
    // jika file ada
    if(strukturIn){
        // baca file sampai end of file
        while(fscanf(strukturIn,"%s %s",table,ret) != EOF){
            if(!strcmp(table_name,table))i = k;
            k++;
        }
        if(i == -1){
            // tutup file
            fclose(strukturIn);
            return -4;
        }
        // tutup file
        fclose(strukturIn);
    }
    else{
        printf("ERROR: file not found\n");
        return -3;
    }

    int w = -1;
    if(strlen(input[4])){
        token = strtok(input[5],"="); // ambil token
        strcpy(banding,token); // copy token
        token = strtok(NULL,"="); // ambil token
        strcpy(cmpwhere,token); // copy token
        k=0;
        // commpare input dengan command WHERE
        if(!strcmp(input[4],"WHERE")){
            // open file struk
            strukturIn = fopen(struk,"r");
            // baca isi file sampai end of file
            while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                if(!strcmp(banding,temp)) {w = k;}
                k++;
            }
            if(w == -1){
                // tutup file
                fclose(strukturIn);
                return -4;
            }
            // tutup file
            fclose(strukturIn);
        }
    }

    // path file
    sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[1]);
    sprintf(n_path,"%s/databases/%s/%s2.txt",databasePath,useDatabase,input[1]);
    
    // open file path
    FILE *file_in = fopen(path,"r");
    // open file n_path
    FILE *file_out = fopen(n_path,"w");
    char ambil[1000];
    char lama[1000];
    // jika file ada
    if(file_in){
        while(fgets(ambil,1000,file_in)){
            strcpy(lama,ambil); // copy
            token = strtok(ambil,",");
            // inisialisasi
            int j = 0, flag = 0;
            char baru[1000];
            strcpy(baru,""); // copy
            
            // perulangan selama token tidak null
            while(token!=NULL){
                if(j == w){
                    if(!strcmp(cmpwhere,token))flag = 1;
                }
                if(j==i){
                    strcat(baru,cmp); // print cmp di akhir baru
                    strcat(baru,","); // print ","di akhir baru
                }
                else{
                    strcat(baru,token); // print token di akhir baru
                    strcat(baru,","); // print ","di akhir baru
                }
                token = strtok(NULL,",");
                j++;
            }
            if(w == -1 || flag == 1){
                baru[strlen(baru)-1] = 0;
                // print baru ke dalam file 
                fprintf(file_out,"%s",baru);
                if(baru[strlen(baru)-1] != '\n'){fprintf(file_out,"\n");}
            }
            else{
                lama[strlen(baru)-1] = 0;
                // print lama ke dalam file
                fprintf(file_out,"%s",lama);
                if(lama[strlen(lama)-1] != '\n'){fprintf(file_out,"\n");}
            }
        }
        // tutup file path
        fclose(file_in);
        // tutup file n_path
        fclose(file_out);

        // remove file
        remove(path);
        rename(n_path,path);
        return 1;
    } else{
        printf("ERROR: file not found\n");
    }
}
```
Fungsi update digunakan untuk memperbarui data dalam tabel pada database. Fungsi ini menerima dua argumen, yaitu buffer yang berisi perintah SQL UPDATE dan useDatabase yang menentukan database yang digunakan. Fungsi ini memproses perintah UPDATE dengan memperbarui nilai kolom yang sesuai dengan kondisi yang diberikan. Jika perintah berhasil dieksekusi, fungsi mengembalikan nilai 1. Jika terjadi kesalahan dalam sintaks atau file tidak ditemukan, fungsi mengembalikan nilai negatif (-1, -2, -3, atau -4) yang menunjukkan jenis kesalahan yang terjadi.

Digunakan untuk menjalankan command `UPDATE`.

<br>

## 24. Select Table

```c
int select_table(char *buffer, char *useDatabase, int *newSocket){
    // cek database
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }

    // inisialisasi buffer
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
   
    // pisahkan token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");

    // inisialisasi
    int i=0, w = -1, urut[20], all, k, kasus;
    char input[10][1000],banding[1000],temp[1000],cmpwhere[1000],ret[1000];
    char path[10000],n_path[10000],struk[10000],cmp[20][1000];

    // perulangan selama token tidak kosong
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    FILE *strukturIn;
    if(!strcmp(input[1],"*")){
        // path
        sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[3]);

        // open file
        strukturIn = fopen(struk,"r");

        if(!strukturIn)return -3;
        // tutup file
        fclose(strukturIn);

        all = -1;
        kasus = 2;
        // compare input[2] dengan from
        if(strcmp(input[2],"FROM"))return -2;
        // compare input[5] dengan where
        if(i >= 5 && !strcmp(input[4],"WHERE")){
            token = strtok(input[5],"="); // ambil token
            strcpy(banding,token); // copy token
            token = strtok(NULL,"="); // ambil token
            strcpy(cmpwhere,token); // copy token
            k=0;
            // compare input[4] dengan where
            if(!strcmp(input[4],"WHERE")){
                // open file
                strukturIn = fopen(struk,"r");
                // baca file sampai end of file
                while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                    if(!strcmp(banding,temp)){w = k;}
                    k++;
                }
                if(w == -1){
                    // tutup file
                    fclose(strukturIn);
                    return -4;
                }
                // tutup file
                fclose(strukturIn);
            }
        }
    }
    else{
        kasus = 2;
        while(kasus <= i && strcmp(input[kasus++],"FROM")){ }
        
        if(kasus > i)return -2;
        
        sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[kasus]);
        // open file
        strukturIn = fopen(struk,"r");
        // jika file ada
        if(!strukturIn){
            printf("ka %d %s\n",kasus,struk);
            return -3;
        }

        // tutup file
        fclose(strukturIn);
        all = 0;kasus = 1;
        
        for(int s=0; s<20; s++){
            urut[s] = -1;
        }

        char temp2[1000];
        while(strcmp(input[kasus],"FROM") && kasus < i){
            k=0;
            // open file
            strukturIn = fopen(struk,"r");
            // baca file sampai eof
            while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                strcpy(temp2,temp); // copy
                strcat(temp2,",");
                if(!strcmp(temp2,input[kasus]) || !strcmp(input[kasus],temp)){
                    strcpy(cmp[all],input[kasus]); // copy input
                    urut[all] = k; all++;
                }
                k++;
            }
            if(urut[all-1] == -1 || all == 0){
                // tutup file
                fclose(strukturIn);
                return -4;
            }
            // tutup file
            fclose(strukturIn);
            kasus++;
        }
        if(kasus == i){
            return -3;
        }
        if(!strcmp(input[kasus+2],"WHERE") && (i >= kasus+2)){
            token = strtok(input[kasus+3],"="); // ambil token
            strcpy(banding,token); // copy token
            token = strtok(NULL,"="); // ambil token
            strcpy(cmpwhere,token); // copy token
            k=0;

            if(!strcmp(input[kasus+2],"WHERE")){
                // open file
                strukturIn = fopen(struk,"r");
                // baca file sampai eof
                while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                    if(!strcmp(banding,temp))w = k;
                    k++;
                }
                if(w == -1){
                    // tutup file
                    fclose(strukturIn);
                    return -4;
                }
                // tutup file
                fclose(strukturIn);
            }
        }
    }

    // path
    sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[kasus+1]);
    sprintf(n_path,"%s/databases/%s/%s2.txt",databasePath,useDatabase,input[kasus+1]);
    
    // open file pada path
    FILE *file_in = fopen(path,"r");
    // open file pada n_path
    FILE *file_out = fopen(n_path,"w");
    // inisialisasi
    char ambil[1000], lama[1000], baru[1000];

    // jika file ada
    if(file_in){
        // inisialisasi
        char init[100] = {0};
        char confirm[1024]={0};
        
        strcpy(init,"Start"); // copy
        // kirim
        send(*newSocket , init , strlen(init) , 0 );
        // menerima respon
        int readValue = recv( *newSocket , confirm, 1024, 0);
        while(fgets(ambil,1000,file_in)){
            if(all == -1){
                if(w == -1){
                    // inisialisasi
                    char text[1000] = {0},buffer2[1000] = {0};
                    strcpy(text,ambil); // copy
                    // kirim dengan socket
                    send(*newSocket , text , strlen(text) , 0 );
                    // menerima melalui socket
                    readValue = recv( *newSocket , buffer2, 1024, 0);
                }
                else{
                    // inisialisasi
                    char hade[1000];
                    int j = 0,flag = 0;
                    strcpy(hade,ambil); // copy

                    // ambil token
                    token = strtok(ambil,",");
                    
                    strcpy(baru,"");
                    // perulangan selama token tidak null
                    while(token!=NULL){
                        if(j == w){
                            if(!strcmp(cmpwhere,token)){
                                // inisialisasi
                                char text[1000] = {0};
                                char buffer2[1000] = {0};
                                strcpy(text,hade);
                                // kirim
                                send(*newSocket , text , strlen(text) , 0 );
                                // menerima respon
                                readValue = recv( *newSocket , buffer2, 1024, 0);
                                // printf("%s\n",hade);
                                break;
                            }
                        }
                        // ambil token baru
                        token = strtok(NULL,",");
                        j++;
                    }
                }
            }
            else{
                // inisialisasi
                char jadi[1000];
                char hade[1000];
                strcpy(jadi,""); //copy
                // loop
                for(int s = 0; s < all; s++){
                    strcpy(hade,ambil); // copy
                    token = strtok(hade,",");
                    int j = 0, flag = 0;

                    strcpy(baru,""); //copy
                    while(token!=NULL){
                        if(j == urut[s]){
                            strcat(jadi,token);
                            strcat(jadi,",");
                            break;
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                }
                // inisialisasi
                char text[1000] = {0};
                char buffer2[1000] = {0};
                jadi[strlen(jadi)-1]='\0';

                // copy jadi ke dalam text
                strcpy(text,jadi);
                // kirim
                send(*newSocket , text , strlen(text) , 0 ); 
                //  menerima respon
                readValue = recv( *newSocket , buffer2, 1024, 0);
            }
        }
        // inisialisasi
        char text[1000] = {0};
        char buffer2[1024] = {0};
        strcpy(text, "EXIT");
        // kirim
        send(*newSocket , text , strlen(text) , 0 );
        // menerima respon
        readValue = recv( *newSocket , buffer2, 1024, 0);
    }
    else{
        return -2;
    }
}
```
Kode program ini adalah implementasi fungsi select_table yang melakukan operasi SELECT pada tabel database. Ia memproses perintah SELECT dari buffer, memeriksa keberadaan tabel dan kolom yang diminta, membuka file tabel, dan mengirimkan baris-baris hasil ke soket yang terhubung. Jika ada kondisi WHERE, hanya baris yang memenuhi kondisi tersebut yang dikirimkan. Program mengirim sinyal "EXIT" setelah selesai.

Digunakan untuk menjalankan command `SELECT` 

<br>

## 25. Run Funtion

```c
void *run(void *arg){
    // inisialisasi
    bool dump_client = false;
    int readValue;
    int *newSocket = (int *) arg;
    char type[1024] = {0};
    char tmptype[1024] = {0};
    char login_user[100] = {0};
    char useDatabase[100]={0};
    readValue = recv( *newSocket , type, 1024, 0);
    
    strcpy(tmptype,type);
    // cek root
    if(!strncmp(type,"root",4)){
        strcpy(login_user,"root");
        char status_login[1000] = {0};
        strcpy(status_login,"Authentication success");
        send(*newSocket , status_login , strlen(status_login) , 0 );

        char *ret = strstr(tmptype,"dump");
        if(ret){
            dump_client = true;
        }
    }
    else{
        //cek login user
        int masuk = login(type,login_user);
        char status_login[1000] = {0};
        
        // jika gagal login
        if(masuk == 0){
            strcpy(status_login,"ERROR: Authentication failed");
            send(*newSocket , status_login , strlen(status_login) , 0 );
            printf("ERROR: Login failed\n");
            close(*newSocket);
            return;
        }
        strcpy(status_login,"Authentication success");
        send(*newSocket , status_login , strlen(status_login) , 0 );

        printf("Login success%s\n",login_user);

        char *ret = strstr(tmptype,"client_dump");
        if(ret){
            dump_client = true;
        }
    }

    if(dump_client){
        char buffer[1024] = {0};
        char message[1024] = {0};

        readValue = recv( *newSocket , buffer, 1024, 0);
        if(!strncmp(buffer,"USE",3)){
            save_log(login_user, buffer);
            int status = use(buffer,type,login_user,useDatabase);
            // jika database tidak ditemukan
            if(status == -2){
                strcpy(message,"ERROR: Unknown database");
                close(*newSocket);
            }else if(status == -1){ //syntax error
                strcpy(message,"ERROR: Syntax error");
                close(*newSocket);
            }else if(status == 1){
                sprintf(message,"ERROR: Using database: %s",useDatabase);
            }else if(status == 0){ //permission denied
                strcpy(message,"ERROR: Permission denied");
                close(*newSocket);
            }
            // kirim
            send(*newSocket , message , strlen(message) , 0 );
            // menerima respon
            readValue = recv( *newSocket , buffer, 1024, 0);
            //generate command
            generate_command(newSocket,useDatabase);
        }
        close(*newSocket);
        return 0;
    }
    while(1){
        char buffer[1024] = {0};
        char message[1024] = {0};
        // test
        char *hello = "Hello, this is from server!";

        readValue = recv( *newSocket , buffer, 1024, 0);
        printf("%s\n", buffer);

        // cek koneksi
        if(isConnectionClosed(readValue,newSocket)){
            printf("ERROR: Connection error\n");
            break;
        }

        // create user
        if(!strncmp(buffer,"CREATE USER",11)){
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = create_user(buffer,type);
            if(status == -2) strcpy(message,"ERROR: User already exist");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
            else if(status == 1) strcpy(message,"Create user success");
            
        }else if(!strncmp(buffer,"USE",3)){ // USE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = use(buffer,type,login_user,useDatabase);
            if(status == -2) strcpy(message,"ERROR: Unknown database");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) sprintf(message,"Using database: %s",useDatabase);
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
        
        }else if(!strncmp(buffer,"CREATE DATABASE",15)){ // CREATE DATABASE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = create_database(buffer,type,login_user);
            if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) strcpy(message,"Create success");
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
            
        }else if(!strncmp(buffer,"GRANT PERMISSION",16)){ // GRANT PERMISSION
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = grant_pemission(buffer,type);
            if(status == -3) strcpy(message,"ERROR: Unknown user");
            else if(status == -2) strcpy(message,"ERROR: Database not exist");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) strcpy(message,"Grant permission success");
            else if(status == 0) strcpy(message,"ERROR: Grant permission denied");
            
        }else if(!strncmp(buffer,"CREATE TABLE",12)){ // CREATE TABLE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = create_table(buffer,useDatabase);
            if(status == -4) strcpy(message,"ERROR: Invalid column type");
            else if(status == -3) strcpy(message,"ERROR: Missing column type");
            else if(status == -2) strcpy(message,"ERROR: No database used");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) strcpy(message,"Create success");
            else if(status == 0) strcpy(message,"ERROR: Table already exist");
            
        }else if(!strncmp(buffer,"DROP DATABASE",13)){ // DROP DATABASE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = drop_database(buffer,type,login_user,useDatabase);
            if(status == -2) strcpy(message,"ERROR: Unknown database");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) sprintf(message,"Database dropped");
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
            
        }else if(!strncmp(buffer,"INSERT INTO",11)){ // INSERT INTO
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = insert(buffer,useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"Insert success");
                    break;
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -3:
                    strcpy(message,"ERROR: Coloumn count doesnt match");
                    break;
                case -4:
                    strcpy(message,"ERROR: Invalid input");
                    break;
            }
        }else if(!strncmp(buffer,"DROP TABLE",10)){ // DROP TABLE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = drop_table(buffer,useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"Drop table success");
                    break;
                case -1:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -2:
                    strcpy(message,"ERROR: No database used");
                    break;
            }
        }else if(!strncmp(buffer,"DROP COLUMN",11)){ // DROP COLUMN
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = drop_column(buffer,useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"Drop column success");
                    break;
                case -1:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -2:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -3:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
            }
        }else if(!strncmp(buffer,"DELETE FROM",11)){ //DELETE FROM
            int status = delete_from(buffer,useDatabase);
            switch(status){
                case 1:
                    strcpy(message,"Delete success");
                    break;
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
                case -3:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -4:
                    strcpy(message,"ERROR: Column does not exist");
                    break;
            }
        }else if(!strncmp(buffer,"UPDATE",6)){
            int status = update(buffer,useDatabase);
            switch(status){
                case 1:
                    strcpy(message,"Update success");
                    break;
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
                case -3:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -4:
                    strcpy(message,"ERROR: Column does not exist");
                    break;
            }
        }else if(!strncmp(buffer,"SELECT",6)){ // SELECT
            int status = select_table(buffer,useDatabase,newSocket);
            switch(status){
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
                case -3:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -4:
                    strcpy(message,"ERROR: Column does not exist");
                    break;
                default:
                    strcpy(message,"Select success");
                    break;
            }
        }else if(!strncmp(buffer,"EXIT", 5)){
            printf("Socket closed\n");
            close(*newSocket);
        }else{
            save_log(login_user, buffer);
            strcpy(message,"ERROR: Syntax error");
        }
        //debug user dan database yg digunakan
        if(!strcmp(buffer,"STATUS")){ //STATUS
            sprintf(message,"login_user:%s useDatabase:%s",login_user,useDatabase);
        } 
        // kirim
        send(*newSocket , message , strlen(message) , 0 );
    }
}
```
- Kode program ini merupakan implementasi dari fungsi run yang berjalan pada thread terpisah. Fungsi ini menerima soket baru sebagai argumen dan memproses perintah yang diterima melalui soket tersebut. Fungsi ini melakukan autentikasi pengguna, memeriksa perintah yang diterima, dan menjalankan operasi terkait seperti membuat pengguna baru, menggunakan database, membuat tabel, menghapus tabel, dan lainnya. Fungsi ini juga menyimpan log aktivitas pengguna dan memberikan respons ke soket yang terhubung. Jika perintah "EXIT" diterima, soket ditutup dan fungsi berakhir.

- Fungsi run digunakan untuk meng-eksekusi seluruh fungsi CRUD yang telah dibuat sebelumnya dan akan mengeluarkan output sesuai dengan return value yang diberikan oleh masing-masing fungsi CRUD.

<br>

## 26. Daemon Process

```c
void daemon_process(){
    // inisialisasi fork
    pid_t child = fork();

    // pengecekan pid
    // gagal
    if(child < 0) {
        exit(EXIT_FAILURE);
    }
    // sukses
    else if(child > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    // inisialisasi session
    pid_t childSID = setsid();

    // pengecekan sid
    // gagal
    if(childSID < 0) {
        exit(EXIT_FAILURE);
    }
    if((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}
```
Kode program ini merupakan implementasi dari fungsi daemon_process yang digunakan untuk menjalankan proses sebagai daemon di sistem. Fungsi ini melakukan beberapa tindakan untuk mengatur proses sebagai daemon sehingga program database.c akan berjalan secara daemon.

<br>

## 27. Main Funtion

```c
int main(int argc, char const *argv[]) {
    // inisialisasi
    // dbpath
    char dbpath[1000] = {0};
    sprintf(dbpath,"%s/databases",databasePath);
    // adminpath
    char adminpath[1000] = {0};
    sprintf(adminpath,"%s/administrator",dbpath);

    // mkdir
    mkdir(dbpath,0777);
    mkdir(adminpath,0777);

    // inisialisasi socket
    int server_fd, newSocket[MAX_USER];
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("ERROR: Socket failed");
        exit(EXIT_FAILURE);
    }

    int reuseaddr = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)) < 0) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    } 

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    // daemon process
    daemon_process();

    int ctr = 0;
    while(1){
        if ((newSocket[ctr] = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("Accept\n");
            exit(EXIT_FAILURE);
        }
        // create thread
        pthread_create(&(tid[ctr]),NULL,run,&newSocket[ctr]);
        ctr++;
        printf("Client %d connected\n", ctr);
    }
    return 0;
}
```
- Pada fungsi main, program melakukan inisialisasi socket, mengikat (bind) socket ke alamat tertentu, dan mendengarkan (listen) koneksi masuk dari client. Setelah itu, program memanggil fungsi daemon_process() untuk menjalankan proses sebagai daemon, yang akan berjalan di background secara terus-menerus.

- Selanjutnya, program masuk ke dalam loop utama yang akan terus menerima koneksi dari client. Setiap kali ada koneksi masuk, program akan menerima koneksi tersebut dan membuat thread baru dengan menggunakan fungsi pthread_create. Thread tersebut akan menjalankan fungsi run yang akan menangani komunikasi dengan client.

- Dengan demikian, program ini berfungsi sebagai server yang siap menerima koneksi dari client, menjalankan thread untuk setiap koneksi yang masuk, dan menangani permintaan dari client secara konkuren.

<br>

---

# Penjelasan Kode Client 

## 1. Library

```c
#include <arpa/inet.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
```
Kode tersebut adalah header dan konstanta yang digunakan dalam sebuah program socket pada client.c yang tehubung dengan database.c .

<br>

## 2. Port

```c
#define PORT 8080
```
Baris ini mendefinisikan konstanta PORT dengan nilai 8080 dan MAX_USER dengan nilai 1000. Konstanta ini digunakan untuk menentukan nomor port yang akan digunakan dalam koneksi socket.

<br>

## 3. Fungsi Main

```c
int main(int argc, char const *argv[]){
	
	struct sockaddr_in clientAddress;
	struct sockaddr_in serverAddress;
	int newSock = 0;
	int readValue;
	bool isDataFromSecondarySource = false;
	char type[1024];
	char statusLogin[1000] = {0};
	int receivedBytes;
	
	if((newSock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		printf("\n ERROR: Socket creation failed \n");
		return -1;
	}
	
	
	memset(&serverAddress, '0', sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(PORT);
	
	if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0){
		printf("\nERROR: Address not found\n");
		return -1;
	}
	
	if(connect(newSock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0){
		printf("\nERROR: socket connection failed\n");
		return -1;
	}
	
	if(getuid()){
	
		if(argc < 5){
			printf("ERROR: too few arguments in function call\n");
			return -1;
		}
		
		if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p")){
			printf("ERROR: syntax error\n");
			return -1;
		}
		
		if(argc == 7){
			if(!strcmp(argv[5], "-d")){
				isDataFromSecondarySource = true;
			} 
			else {
				printf("ERROR: syntax error\n");
				return -1;
			}
		}
	} 
	else {
		if(argc == 3){
			if(!strcmp(argv[1],"-d")){
				isDataFromSecondarySource = true;
			} 
			else {
				printf("ERROR: syntax error\n");
				return -1;
			}
		}
	}
	
	if(getuid()){
		strcpy(type, argv[2]);
		strcat(type, " ");
		strcat(type, argv[4]);
		send(newSock, type, strlen(type), 0);
	} else {
		strcpy(type, "root");
		send(newSock, type, strlen(type), 0);
	}
	
	receivedBytes = recv(newSock, statusLogin, 1000, 0);
	printf("%s\n", statusLogin);
	if(!strcmp(statusLogin, "ERROR: Authentication failed")){
		close(newSock);
		return -1;
	}
	
	if(getuid()){
		
		if(isDataFromSecondarySource){
			char cmd[1000] = {0};
			char receivedData[1000] = {0};
			sprintf(cmd, "USE %s", argv[6]);
			send(newSock, cmd, strlen(cmd), 0);
			receivedBytes = recv(newSock, receivedData, 1000, 0);
			if(!strncmp(receivedData, "Using database: ", 19)){
				char temp[1000];
				while((fscanf(stdin, "%[^\n]%*c", temp)) != EOF){
				    send(newSock, temp, strlen(temp), 0);
				    bzero(receivedData, sizeof(receivedData));
					receivedBytes = recv(newSock, receivedData, 1000, 0);
					printf("%s\n", receivedData);
				};	
		    	close(newSock);
				return 0;
			}
		}
	} 
	else {
		if(isDataFromSecondarySource) {
			char cmd[1000] = {0};
			char receivedData[1000] = {0};
			sprintf(cmd, "USE %s", argv[2]);
			send(newSock, cmd, strlen(cmd), 0);
			receivedBytes = recv(newSock, receivedData, 1000, 0);
			if(!strncmp(receivedData, "Using database: ", 19)){	
				char temp[1000]; 
				while((fscanf(stdin, "%[^\n]%*c", temp)) != EOF){
					send(newSock, temp, strlen(temp), 0);
					bzero(receivedData, sizeof(receivedData));
					receivedBytes = recv(newSock, receivedData, 1000, 0);
					printf("%s\n", receivedData);
				};
				close(newSock);
				return 0;
			}
		} 
	}
	
	while(true){
	
		char message[1000] = {0};
		char buffer[1024] = {0};
		gets(message);
		send(newSock, message, strlen(message), 0);
		readValue = recv(newSock, buffer, 1024, 0);
		if(isConnectionClosed(readValue, newSock)){
			break;
		}
		if(!strcmp(buffer, "BEGIN")){
			char cmd[1000] = {0};
			char receivedData[1000] = {0};
			strcpy(cmd, "ok");
			send(newSock, cmd, strlen(cmd), 0);
			do{
				bzero(receivedData, sizeof(receivedData));
				receivedBytes = recv(newSock, receivedData, 1000, 0);
				if(strcmp(receivedData, "SUCCESS")){
					if(receivedData[strlen(receivedData) -1] != '\n'){
						printf("%s\n", receivedData);
					}
					else{
						printf("%s", receivedData);
					}
					bzero(cmd, sizeof(cmd));
					strcpy(cmd, "ok");
					send(newSock, cmd, strlen(cmd), 0);
				}
			}
			while(strcmp(receivedData, "SUCCESS"));
			bzero(cmd, sizeof(cmd));
			strcpy(cmd, "ok");
			send(newSock, cmd, strlen(cmd), 0);
			receivedBytes = recv(newSock, buffer, 1024, 0);
			printf("%s\n", buffer);
			
		} 
		else {
			printf("%s\n", buffer);
		}
	}
	return 0;
}
```
- Kode program ini merupakan implementasi client socket yang melakukan koneksi ke server socket. Pada fungsi main, program membuat socket baru dan menghubungkannya ke alamat server menggunakan fungsi connect. Kemudian program melakukan pengiriman pesan ke server yang berisi jenis dan argumen dari perintah yang akan dijalankan.

- Setelah itu, program menerima pesan balasan dari server yang berisi status hasil autentikasi. Jika autentikasi berhasil, program akan melanjutkan dengan mengirimkan perintah-perintah ke server dan menerima balasan dari server. Jika terdapat flag -d pada argumen command line, program akan membaca perintah-perintah dari stdin dan mengirimkannya ke server. Jika tidak terdapat flag -d, program akan meminta input perintah dari pengguna dan mengirimkannya ke server.

- Selama berlangsungnya koneksi, program akan terus mengirim dan menerima pesan dengan server. Jika pesan yang diterima berisi "BEGIN", program akan melakukan komunikasi lebih lanjut dengan server untuk menjalankan transaksi database, dengan mengirimkan perintah-perintah dan menerima balasan dari server.

- Dengan demikian, program ini berfungsi sebagai client yang berkomunikasi dengan server menggunakan socket, mengirimkan perintah-perintah ke server, dan menerima balasan dari server.

<br>


## 4. Check Connection Function

```c
bool isConnectionClosed(int readValue, int newSock){
	if(!readValue){
		close(newSock);
		return true;
	}
	return false;
}
```

- Kode program ini mendefinisikan fungsi isConnectionClosed yang digunakan untuk memeriksa apakah koneksi socket telah ditutup. Fungsi ini menerima dua parameter, yaitu readValue yang merupakan nilai kembalian dari fungsi recv yang mengindikasikan jumlah byte yang berhasil dibaca, dan newSock yang merupakan file descriptor dari socket.

- Fungsi ini akan mengembalikan nilai true jika readValue bernilai 0, yang menunjukkan bahwa tidak ada data yang berhasil dibaca, dan dalam hal ini koneksi socket dianggap ditutup. Fungsi akan melakukan penutupan socket dengan menggunakan fungsi close dan mengembalikan true.

- Jika readValue bukan 0, fungsi akan mengembalikan nilai false, menunjukkan bahwa koneksi socket masih terbuka.

<br>

---
# Penjelasan Kode Client_dump 

## 1. Library

```c
#include <arpa/inet.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
```
Kode tersebut adalah header dan library yang digunakan dalam sebuah program dump. Program "dump" merujuk pada proses pengambilan atau penyalinan data yang dikirim atau diterima melalui socket

<br>

## 2. Port

```c
#define PORT 8080
```
Baris ini mendefinisikan konstanta PORT dengan nilai 8080 dan MAX_USER dengan nilai 1000. Konstanta ini digunakan untuk menentukan nomor port yang akan digunakan dalam koneksi socket.

<br>

## 3. Fungsi Main

```c
int main(int argc, char const *argv[]){
	struct sockaddr_in clientAddress;
	struct sockaddr_in serverAddress;
	int newSock = 0;
	int newResponse;
	char type[1024];
	char statusLogin[1000] = {0};
	int receivedBytes;

	if(getuid()){
		if(argc < 6) {
			printf("Terjadi kesalahan pada sintaksis kode\n");
			return -1;
		} 
		else {
			if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p")){
				return -1;
			}
		}
	} 
	else  {
		if(argc < 2){
			printf("Terjadi kesalahan pada sintaksis kode\n");
			return -1;
		}
	}
	
	if((newSock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Terjadi kesalahan dalam pembuatan soket \n");
		return -1;
	}
	
	
	memset(&serverAddress, '0', sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(PORT);
	
	if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0){
		printf("\n Alamat tidak valid \n");
		return -1;
	}
	if(connect(newSock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0){
		printf("\n Gagal melakukan koneksi \n");
		return -1;
	}

	if(getuid()){
		strcpy(type, argv[2]);
		strcat(type, " ");
		strcat(type, argv[4]);
		strcat(type, " dump ");
		send(newSock, type, strlen(type), 0);
	} 

	else {
		strcpy(type, "root dump ");
		send(newSock, type, strlen(type), 0);
	}
	
	receivedBytes = recv(newSock, statusLogin, 1000, 0);
	printf("%s\n", statusLogin);
	if(!strcmp(statusLogin, "Authentication failed")){
		close(newSock);
		return -1;
	}
	
	if(getuid()){
		char cmd[1000] = {0};
		char receivedData[1000] = {0};
		sprintf(cmd, "USE %s", argv[5]);
		send(newSock, cmd, strlen(cmd), 0);
		receivedBytes = recv(newSock, receivedData, 1000, 0);
		bzero(cmd, sizeof(cmd));
		strcpy(cmd,"ok");
		send(newSock, cmd, strlen(cmd), 0);
		if(!strncmp(receivedData, "database changed to", 19)){
			do{
				bzero(receivedData, sizeof(receivedData));
				receivedBytes = recv(newSock, receivedData, 1000, 0);
				if(strcmp(receivedData, "DONE!!!")){a
					printf("%s\n", receivedData);
					bzero(cmd, sizeof(cmd));
					strcpy(cmd,"ok");
					send(newSock, cmd, strlen(cmd), 0);
				}
			}
			while(strcmp(receivedData, "DONE!!!"));
			close(newSock);
			return 0;
		}
		
	} else {
		char cmd[1000] = {0};
		char receivedData[1000] = {0};
		sprintf(cmd, "USE %s", argv[5]);
		send(newSock, cmd, strlen(cmd), 0);
		receivedBytes = recv(newSock, receivedData, 1000, 0);
		bzero(cmd, sizeof(cmd));
		strcpy(cmd,"ok");
		send(newSock, cmd, strlen(cmd), 0);
		if(!strncmp(receivedData, "database changed to", 19)){
			do{
				bzero(receivedData, sizeof(receivedData));
				receivedBytes = recv(newSock, receivedData, 1000, 0);
				if(strcmp(receivedData, "DONE!!!")){
					printf("%s\n", receivedData);
					bzero(cmd, sizeof(cmd));
					strcpy(cmd,"ok");
                    send(newSock, cmd, strlen(cmd), 0);
				}
			}
			while(strcmp(receivedData, "DONE!!!"));
			close(newSock);
			return 0;
		}
	}
	return 0;
}
```

- Program tersebut adalah sebuah program client yang menggunakan socket untuk melakukan koneksi dengan server. Tujuan program ini adalah untuk mengirim perintah ke server dan menerima respons dari server. Program ini menerima argumen dari baris perintah saat dijalankan.

- Pertama, program membuat sebuah soket baru menggunakan fungsi socket(). Setelah itu, program menginisialisasi struktur alamat server dan melakukan koneksi dengan server menggunakan fungsi connect(). Jika koneksi berhasil, program mengirimkan perintah ke server berdasarkan argumen yang diberikan. Perintah ini berisi jenis operasi (misalnya, "root" atau "user"), data pengguna, dan perintah "dump" untuk meminta dump data.

- Setelah mengirim perintah, program menunggu respons dari server menggunakan fungsi recv(). Respons ini kemudian dicetak ke output. Program juga melakukan autentikasi dengan server dan memeriksa status login. Jika autentikasi gagal, program menutup soket dan mengembalikan nilai -1.

- Jika autentikasi berhasil, program mengirim perintah "USE" ke server untuk mengubah basis data yang aktif di server. Setelah mengirim perintah, program kembali menunggu respons dari server dan mencetaknya ke output. Jika respons tersebut bukan "DONE!!!", program mengirimkan perintah "ok" ke server untuk melanjutkan pengiriman data.

- Langkah-langkah di atas diulang sampai respons dari server adalah "DONE!!!", yang menandakan bahwa pengiriman data telah selesai. Setelah itu, program menutup soket dan mengembalikan nilai 0.

- Secara keseluruhan, program ini berfungsi sebagai client yang berinteraksi dengan server menggunakan socket. Program ini memungkinkan pengguna untuk mengirimkan perintah ke server dan menerima responsnya, serta melakukan operasi "dump" untuk mendapatkan data dari server.

<br>

## 4. Check Connection 

```c
bool isConnectionClosed(int newResponse, int newSock){
	if(newResponse == 0){
		close(newSock);
		return true;
	}
	return false;
}
```
- Fungsi isConnectionClosed digunakan untuk memeriksa apakah koneksi dengan soket tertentu telah ditutup atau tidak. Fungsi ini mengambil dua parameter, yaitu newResponse yang merupakan nilai yang diterima dari pemanggilan fungsi recv(), dan newSock yang merupakan soket yang ingin diperiksa.

- Dalam fungsi ini, jika newResponse memiliki nilai 0, artinya tidak ada data yang diterima dari soket tersebut. Hal ini menunjukkan bahwa koneksi telah ditutup oleh pihak lain. Dalam kasus ini, fungsi isConnectionClosed akan menutup soket menggunakan fungsi close() dan mengembalikan nilai true untuk menandakan bahwa koneksi telah ditutup.

- Jika newResponse bukan nilai 0, fungsi akan mengembalikan nilai false untuk menunjukkan bahwa koneksi masih terbuka.

- Dengan menggunakan fungsi isConnectionClosed, program dapat memeriksa apakah koneksi dengan soket tertentu masih aktif atau sudah ditutup, sehingga dapat mengambil tindakan yang sesuai sesuai dengan status koneksi tersebut.

<br>

---
# Penjelasan Cron.sh
## Berikut merupakan kode program cron.sh
```c
#!/bin/bash

# Buat file sementara untuk menyimpan jadwal cron
cron_file=$(mktemp)

# Isi file sementara dengan jadwal cron yaitu akan dijalankan setiap 1 jam
echo "0 */1 * * * cd /home/sarahnrhsna/Documents/sisop/fp/database && /usr/bin/zip -rm `date \"+\%Y-\%m-\%d\"`\ `date +\"\%H:\%M:\%S\"` dblog.log *.backup" > $cron_file

# Impor jadwal cron dari file sementara
crontab $cron_file

# Hapus file sementara
rm $cron_file
```
Program cron.sh digunakan untuk menjalankan program client_dump setiap jam dan file akan di zip sesuai timestamp 


<br>

---
# Penjelasan Docker

## 1. Buat Dockerfile
```c
# Base image
FROM ubuntu:latest

# Install necessary packages
RUN apt-get update && \
    apt-get install -y gcc sudo
RUN apt-get update && apt-get install -y cron

# Create a new user and set it as the working user
RUN useradd -ms /bin/bash myuser

# Set the working directory
WORKDIR /home/myuser

# Copy the source code
COPY database.c .
COPY client.c .

# Compile the programs
RUN gcc -pthread -o database database.c
RUN gcc -o client client.c

# Set the entry point to run the database and client programs
CMD ["bash", "-c", "./database & sleep 7 && sudo ./client"]
```
- Install package yang diperlukan:
    ```
    RUN apt-get update && \
    apt-get install -y gcc sudo
    RUN apt-get update && apt-get install -y cron
    ```
- Copy source code untuk program database.c dan client.c
    ```
    COPY database.c .
    COPY client.c .
    ```
- Run program database dan client
    ```
    CMD ["bash", "-c", "./database & sleep 7 && sudo ./client"]
    ```
- Build dockerfile dengan command berikut untuk membuat docker image dengan nama `database-app`:
    ```
    sudo docker build -t database-app
    ```
- Run docker image dengan menggunakan perintah berikut:
    ```
    sudo docker run database-app`
    ```

<br>

## 2. Buat Docker hub
### a. Login docker dengan perintah berikut:
```
sudo docker login
```
### b. Buat tag dengan perintah berikut:
```
docker tag <nama_image>:<version_image> <nama_repository>/<nama_image>:<version_image>
```
dalam kasus saya, saya menggunakan perintah:
```
sudo docker tag database-app sarahnrhsna/database-app
```

<br>

## 3. Buat Docker Compose
```c
version: '3'
services:
  sukolilo:
    build: ./sukolilo
    image: sarahnrhsna/database-app
    ports:
      - 8080-8084:8080
    deploy:
      replicas: 5
  keputih:
    build: ./keputih
    image: sarahnrhsna/database-app 
    ports:
      - 8085-8089:8080
    deploy:
      replicas: 5
  gebang:
    build: ./gebang
    image: sarahnrhsna/database-app
    ports:
      - 8090-8094:8080
    deploy:
      replicas: 5
  mulyos:
    build: ./mulyos
    image: sarahnrhsna/database-app
    ports:
      - 8095-8099:8080
    deploy:
      replicas: 5
  semolowaru:
    build: ./semolowaru
    image: sarahnrhsna/database-app
    ports: 
      - 8100-8104:8080
    deploy:
      replicas: 5
```
- Docker compose di atas menyediakan 5 service yaitu sukolilo, keputih, gebang, mulyos, dan semolowaru.

- Masing-masing service membuat 5 instance dengan portnya masing-masing.

<br>

# Hasil Screenshot Program
## 1. database.c
![database](img/DATABASE.png)

<br>

## 2. client.c
### a. Autentikasi
![autentikasi](img/AUTENTIFIKASI.png)

<br>

### b. Autorisasi
![autorisasi](img/AUTORISASI.png)
<br>

### c. Data Definition Language
- DDL CREATE
![CREATE](img/DDL_CREATE.png)

- DDL DROP COLUMN
![DROP_COLUMN](img/DDL_DROP_COLUMN.png)
- DDL DROP TABLE
![CDROP_TABLE](img/DDL_DROP_TABLE.png)
- DDL DROP DATABASE
![DROP_DATABASE](img/DDL_DROP_DATABASE.png)
<br>

### d. Data Manipulation Language
![DML](img/DML.png)
<br>

### e. Logging
![logging](img/LOGGING.png)
<br>

### f. Error Handling
![error_handling](img/ERROR_HANDLING.png)
<br>


## 3. client_dump.c
![client_dump](img/CLIENT_DUMP.png)

<br>

## 4. cron.sh
- Hasil cron
![cron](img/HASIL_CRON.png)
- Crontab
![crontab](img/CRONTAB.png)

<br>

## 5. Docker
### a. Docker image
![docker_image](img/DOCKER.png)
<br>

### b. Docker Hub
- push docker
![docker-hub](img/DOCKER-HUB.png)

- hasil docker hub
![hasildockerhub](img/docker_hub2.png)
<br>

### c. Docker Compose
![docker-compose](img/DOCKER-COMPOSE.png)
<br>