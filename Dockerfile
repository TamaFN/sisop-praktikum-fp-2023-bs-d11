# Base image
FROM ubuntu:latest

# Install necessary packages
RUN apt-get update && \
    apt-get install -y gcc sudo
RUN apt-get update && apt-get install -y cron

# Create a new user and set it as the working user
RUN useradd -ms /bin/bash myuser

# Set the working directory
WORKDIR /home/myuser

# Copy the source code
COPY database.c .
COPY client.c .

# Compile the programs
RUN gcc -pthread -o database database.c
RUN gcc -o client client.c

# Set the entry point to run the database and client programs
CMD ["bash", "-c", "./database & sleep 7 && sudo ./client"]
