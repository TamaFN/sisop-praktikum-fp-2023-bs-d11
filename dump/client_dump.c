// Impor Header yang akan digunakan
#include <arpa/inet.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>

// Definisi PORT
#define PORT 8080

// Deklarasi Fungsi yang digunakan 
bool isConnectionClosed(int newResponse, int newSock);


// Main Program
int main(int argc, char const *argv[]){
	
	// Digunakan untuk menyimpan informasi alamat client
	struct sockaddr_in clientAddress;
	
	// Digunakan untuk menyimpan informasi alamat server
	struct sockaddr_in serverAddress;
	
	// Digunakan untuk berkomunikasi dengan client
	int newSock = 0;
	
	// Digunakan untuk menyimpan respons dari koneksi atau operasi lainnya
	int newResponse;
	
	// Inisialisasi array of char untuk variable type
	char type[1024];
	
	// Inisialisiasi array of char untuk variable statusLogin
	char statusLogin[1000] = {0};
	
	// Inisialisasi varibale untuk menyimpan jumlah byte yang diterima
	int receivedBytes;
	
	
	// Mengecek apakah getuid tidak bernilai 0
	if(getuid()){
	// Kondisi Terpenuhi
	
		// Memeriksa apakah jumlah argumen kurang dari 6
		if(argc < 6) {
			// Kondisi terpenuhi
			printf("Terjadi kesalahan pada sintaksis kode\n");
			
			// Menandakan adanya kesalahan
			return -1;
		} 
		// Kondisi tidak terpenuhi
		else {
			/* Memeriksa apakah argumen pada indeks 1 adalah "-u" 
		   	   dan argumen pada indeks 3 adalah "=p" */ 
			if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p")){
				
				// Kondisi terpenuhi
				// Menandakan adanya kesalahan
				return -1;
			}
		}
	} 
	// Kondisi tidak terpenuhi
	else  {
		// Memeriksa apakah jumlah argumen kurang dari 2
		if(argc < 2){
		// Kondisi terpenuhi
			// Mencetak pesan
			printf("Terjadi kesalahan pada sintaksis kode\n");
			
			// Menandakan adanya kesalahan
			return -1;
		}
	}
	
	
	// Mengecek apakah newSock < 0
	if((newSock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		// Kondisi terpenuhi
		printf("\n Terjadi kesalahan dalam pembuatan soket \n");
		
		// Menunjukkan adanya kesalahan dalam pembuatan socket
		return -1;
	}
	
	
	// Digunakan untuk mengatur nilai awal serverAddress dengan nilai 0
	memset(&serverAddress, '0', sizeof(serverAddress));
	
	// Digunakan untuk menetapkan jenis alamat yang akan digunakan
	serverAddress.sin_family = AF_INET;
	
	// Digunakan untuk menentukan nomor port yang akan digunakan pada koneksi
	serverAddress.sin_port = htons(PORT);
	
	
	// Mengecek apakah hasil konversi kurang <= 0
	if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0){
		// Kondisi terpenuhi
		printf("\n Alamat tidak valid \n");
		
		// Menandakan adanya kesalahan dalam konversi alamat IP
		return -1;
	}
	
	// Mengecek apakah koneksi ke server berhasil dilakukan
	if(connect(newSock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0){
		
		// Jika tidak bisa melakukan koneksi ke server
		printf("\n Gagal melakukan koneksi \n");
		
		// Menandakan bahwa koneksi gagal
		return -1;
	}
	
	// Memeriksa apakah getUID() mengembalikan nilai bukan 0
	if(getuid()){
	
		// Menyalin string dari argv[2], ke type
		strcpy(type, argv[2]);
		
		// Menggabungkan spasi ke type
		strcat(type, " ");
		
		// Menggabungkan string dari argv[4] ke type
		strcat(type, argv[4]);
		
		// Menggabungkan string "dump" ke type
		strcat(type, " dump ");
		
		// Mengirimkan data type melalui socket
		send(newSock, type, strlen(type), 0);
	} 
	// Jika getUID() mengembalikan nilai 0
	else {
		// Menyalin string "root dump" ke type
		strcpy(type, "root dump ");
		
		// Mengirimkan data dalam type melalui socket
		send(newSock, type, strlen(type), 0);
	}
	
	// Mengembalikan jumlah byte yang berhasil diterima dan disimpan
	receivedBytes = recv(newSock, statusLogin, 1000, 0);
	
	// Mencetak statusLogin
	printf("%s\n", statusLogin);
	
	// Memeriksa apakah string dalam statusLogin berisi Authentication failed
	if(!strcmp(statusLogin, "Authentication failed")){
		
		// Menutup socket milik kliend
		close(newSock);
		
		// Menandakan adanya kesalahan
		return -1;
	}
	
	// Memeriksa apakah getUID() mengembalikan nilai bukan 0
	if(getuid()){
	// Kondisi Terpenuhi
	
		// Inisialisasi variable cmd
		char cmd[1000] = {0};
			
		// Inisialisai variable receivedData
		char receivedData[1000] = {0};
		
		// Mengisi array cmd
		sprintf(cmd, "USE %s", argv[5]);
		
		// Mengirimkan data pada cmd melalui socket
		send(newSock, cmd, strlen(cmd), 0);
		
		// Menerima data yang dikirimkan lewat socket berupa jumlah byte
		receivedBytes = recv(newSock, receivedData, 1000, 0);
		
		// Menghapus isi cmd atau mengisi dengan nilai 0
		bzero(cmd, sizeof(cmd));
		
		// Menyalin string "ok" ke cmd
		strcpy(cmd,"ok");
		
		// Mengirimkan data pada cmd melalui socket
		send(newSock, cmd, strlen(cmd), 0);
		
		// Memeriksa apakah string yang diterima = database changed to sepanjang 19 karakter
		if(!strncmp(receivedData, "database changed to", 19)){
			// Memulai loop do-while untuk menerima dan memproses data sampai menerima string "SUCCES"
			do{
				// Mengisi array receivedData dengan 0
				bzero(receivedData, sizeof(receivedData));
				
				// menerima data dari newSock dan menyimpannya dalam receivedData
				receivedBytes = recv(newSock, receivedData, 1000, 0);
				
				// Memeriksa apakah string dalam receivedData 
				if(strcmp(receivedData, "DONE!!!")){
				// Kondisi terpenuhi
					
					// Mencetak isi receivedData
					printf("%s\n", receivedData);
					
					// Menghapus isi cmd atau mengisi dengan nilai 0
					bzero(cmd, sizeof(cmd));
					
					// Menyalin string "ok" ke cmd
					strcpy(cmd,"ok");
					
					// Mengirimkan data pada cmd melalui socket
					send(newSock, cmd, strlen(cmd), 0);
				}
			}
			
			// Kembali ke awal loop dan melanjutkan proses hinggal string receivedData adalah "SUCCESS"
			while(strcmp(receivedData, "DONE!!!"));
			
			// Menutup socket
			close(newSock);
			
			// Mengembalikan nilai 0
			return 0;
		}
		
	} 
	// Jika kondisi tidak terpenuhi
	else {
				// Inisialisasi variable cmd
		char cmd[1000] = {0};
			
		// Inisialisai variable receivedData
		char receivedData[1000] = {0};
		
		// Mengisi array cmd
		sprintf(cmd, "USE %s", argv[5]);
		
		// Mengirimkan data pada cmd melalui socket
		send(newSock, cmd, strlen(cmd), 0);
		
		// Menerima data yang dikirimkan lewat socket berupa jumlah byte
		receivedBytes = recv(newSock, receivedData, 1000, 0);
		
		// Menghapus isi cmd atau mengisi dengan nilai 0
		bzero(cmd, sizeof(cmd));
		
		// Menyalin string "ok" ke cmd
		strcpy(cmd,"ok");
		
		// Mengirimkan data pada cmd melalui socket
		send(newSock, cmd, strlen(cmd), 0);
		
		// Memeriksa apakah string yang diterima = database changed to sepanjang 19 karakter
		if(!strncmp(receivedData, "database changed to", 19)){
			// Memulai loop do-while untuk menerima dan memproses data sampai menerima string "SUCCES"
			do{
				// Mengisi array receivedData dengan 0
				bzero(receivedData, sizeof(receivedData));
				
				// menerima data dari newSock dan menyimpannya dalam receivedData
				receivedBytes = recv(newSock, receivedData, 1000, 0);
				
				// Memeriksa apakah string dalam receivedData 
				if(strcmp(receivedData, "DONE!!!")){
				// Kondisi terpenuhi
					
					// Mencetak isi receivedData
					printf("%s\n", receivedData);
					
					// Menghapus isi cmd atau mengisi dengan nilai 0
					bzero(cmd, sizeof(cmd));
					
					// Menyalin string "ok" ke cmd
					strcpy(cmd,"ok");
					
					// Mengirimkan data pada cmd melalui socket
					send(newSock, cmd, strlen(cmd), 0);
				}
			}
			
			// Kembali ke awal loop dan melanjutkan proses hinggal string receivedData adalah "SUCCESS"
			while(strcmp(receivedData, "DONE!!!"));
			
			// Menutup socket
			close(newSock);
			
			// Mengembalikan nilai 0
			return 0;
		}
	}
	return 0;
}






// Mengecek apakah koneksi koneksi sudah ditutup atau masih aktif
bool isConnectionClosed(int newResponse, int newSock){
	
	// Jika newResponse = 0, artinya koneksi ditutup
	if(newResponse == 0){
		// Menutup socket
		close(newSock);
		
		// Mengembalikan nilai true untuk menandakan koneksi ditutup
		return true;
	}
	
	// Mengembalikan nilai false jika koneksi masih aktif
	return false;
}
