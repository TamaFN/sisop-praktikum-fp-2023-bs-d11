#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

// port
#define PORT 8080
#define MAX_USER 1000

// menyimpan path dir database untuk menyimpan file log database dan database.backup
char *databasePath = "/home/sarahnrhsna/Documents/sisop/fp/db_database";
// menyimpan path dir user untuk menyimpan file user.txt
char *userPath = "/home/sarahnrhsna/Documents/sisop/fp/user/user.txt";
// menyimpan path dir database untuk menyimpan file permission.txt
char *permissionPath = "/home/sarahnrhsna/Documents/sisop/fp/db_database/permission.txt";
pthread_t tid[MAX_USER];
// fungsi untuk mengecek ketersediaan socket
bool isConnectionClosed(int readValue, int *newSocket){
    // cek value
    if(readValue == 0){ //jika 0 maka socket tidak tersedia
        printf("Socket closed\n");
        close(*newSocket); //tutup socket
        return 1;
    }
    return 0;
}

// fungsi untuk mengecek permission client
bool check_user(char *user){
    // open file user.txt pada path yang telah ditentukan yaitu userPath
    FILE *fileUser = fopen(userPath,"r");
    // inisialisasi
    char usertmp[1000];
    char passtmp[1000];

    // jika data pada file user.txt tersedia
    if(fileUser){
        // lakukan perulangan
        while(fscanf(fileUser,"%s %s",usertmp,passtmp) != EOF){ //scanf fileUser sampai end of file
            // jika value usertmp tidak sesuai dengan user
            if(!strcmp(usertmp,user)){
                // maka return 1
                printf("ERROR: user not found\n");
                return 1; //artinya permission denied
            }
        }
    }
    return 0;
}

// fungsi untuk mengecek permission database
bool check_database(char *data_base){
    // open file permission.txt pada path yang telah ditentukan yaitu permissionPath
    FILE *filedb = fopen(permissionPath,"r");
    // inisialisasi
    char usertmp[1000];
    char databasetmp[1000];

    // jika data pada file db tersedia
    if(filedb){
        // lakukan perulangan
        while(fscanf(filedb,"%s %s",databasetmp,usertmp) != EOF){ //scanf filedb sampai end of file
            // jika value databasetmp tidak sesuai dengan database
            if(!strcmp(databasetmp,data_base)){ 
                // maka return 1
                return 1; // artinya permission denied
            }
        }
    }
    return 0;
}

// fungsi untuk generate command
void generate_command(int *newSocket, char *useDatabase){
    // buat pointer dp yang mempresentasikan direktori database yang dibuka
    DIR *dp;
    // buat pointer ep yang mempresentasikan setiap entri di dalam direktori
    struct dirent *entry;
    // inisialisasi
    char dbpath[1000] = {0};
    // buat path dengan menggabungkan string databasePath dan userDatabase
    sprintf(dbpath,"%s/databases/%s",databasePath,useDatabase); // gunakan fungsi sprintf untuk menggabungkan string
    // sehingga format path --> databasePath/databases/useDatabase

    // open dir dp dengan path pada dbpath
    dp = opendir(dbpath);

    // jika isi dp tidak null
    if (dp != NULL){
        // MEMBACA ENTRI DIREKTORI
        // lakukan perulangan untuk membaca semua isi/entri dp
        while ((entry = readdir (dp))) {
            if(!strncmp(entry->d_name,"struktur_",9)){ //compare string
                // inisialisasi
                char table_name[1000] = {0}; char temp[1000] = {0};

                // MENDAPATKAN NAMA TABEL
                // copy d_name ke dalam temp
                strcpy(temp,entry->d_name);
                // memisahkan token dengam fungsti strtok
                char *token = strtok(temp,"_");
                token = strtok(NULL,"_");
                // copy token ke dalam variabel table_name
                strcpy(table_name,token);

                // MEMBUKA FILE STRUKTUR TABEL
                // inisialisasi array untuk menyimpan kolom pada tabel
                char kolom[100][1000] = {0}; 
                int jumlah_kolom = 0;

                // inisialisasi path
                char path[1000] = {0};
                // buat path untuk mendapatkan kolom tabel
                sprintf(path,"%s/databases/%s/%s",databasePath,useDatabase,entry->d_name); // gabungkan string dan masukkan ke dalam variabel path dengan menggunakan sprintf
                // sehingga format path --> databasePath/databases/useDatabase/d_name
                // open file
                FILE *file_in = fopen(path,"r");
                // jika file tersedia maka
                if(file_in){
                    // inisialisasi
                    char temp2[1000];
                    // lakukan perulangan untuk membaca isi file
                    while((fscanf(file_in,"%[^\n]%*c",temp2)) != EOF){
                        strcpy(kolom[jumlah_kolom++],temp2);// copy value temp2 ke dalam array kolom
                    }

                    // tutup file
                    fclose(file_in);

                    // inisialisasi
                    int readValue;
                    char command[1000] = {0};
                    char buffer[1024] = {0};
                    char table[1000] = {0};

                    // copy value pada table_name ke dalam variabel table
                    strcpy(table,table_name);
                    // pisahkan string table untuk mendapatkan token yang merupakan kolom-kolom pada tabel
                    char *token_table = strtok(table,"."); //kolom dipisahkan oleh "."
                    
                    // CREATE TABLE
                    sprintf(command,"CREATE TABLE %s (",token_table); //printf command create table
                    // lakukan perulangan sebanyak jumlah kolom
                    for(int i = 0; i < jumlah_kolom; i++){
                        strcat(command,kolom[i]); // memasukkan setiap kolom[i] ke dalam command
                        strcat(command,","); // kolom dipisahkan oleh tanda ","
                    }

                    // tanda tutup kurung untuk command CREATE TABLE
                    command[strlen(command)-1] = ')'; // --> CREATE TABLE ()
                    // command diakhiri oleh ";"
                    strcat(command,";"); // --> CREATE TABLE (kolom1, kolom2);

                    // kirim command dengan socket
                    send(*newSocket , command , strlen(command) , 0 );
                    // menerima respon dari socket
                    readValue = recv( *newSocket , buffer, 1024, 0);

                    // reset command
                    bzero(path,sizeof(path)); // menghapus value command

                    // DROP TABLE
                    sprintf(command,"DROP TABLE %s;",token_table); //printf command drop table
                    // kirim command dengan socket
                    send(*newSocket , command , strlen(command) , 0 );
                    // menerima respon dari socket
                    readValue = recv( *newSocket , buffer, 1024, 0);

                    // reset command
                    bzero(command,sizeof(command)); // menghapus value command

                    // path file tabel yang akan dibaca
                    sprintf(path,"%s/databases/%s/%s",databasePath,useDatabase,table_name); // --> databasePath/databases/useaDatabase/table_name

                    // open file dengan command "r" yaitu untuk membaca isi file
                    file_in = fopen(path,"r");
                    if(file_in){
                        // lakukan perulangan untuk membaca setiap baris file sampai end of file
                        while((fscanf(file_in,"%[^\n]%*c",temp2)) != EOF){
                            // reset command
                            bzero(command,sizeof(command)); // menghapus value command

                            // INSERT INTO _ VALUES _
                            sprintf(command,"INSERT INTO %s VALUES (%s);",token_table,temp2); //printf command INSERT INTO _ VALUES _

                            send(*newSocket , command , strlen(command) , 0 );// kirim command menggunakan socket

                            readValue = recv( *newSocket , buffer, 1024, 0);// menerima respon dari socket
                        }

                        // close file
                        fclose(file_in);
                    }
                }   
            }
        }

        // EXIT COMMAND
        char command[1000] = {0}; //inisialisasi
        // reset command
        bzero(command,sizeof(command)); // menghapus value command
        // copy command exit ke dalam variabel command
        strcpy(command,"EXIT");
        // kirim command dari socket
        send(*newSocket , command , strlen(command) , 0 );

        // close database
        (void) closedir (dp);

    } else {
        printf ("ERROR: Couldn't open the directory");
    }
}

// fungsi untuk menulis teks ke dalam file yang ditentukan oleh path
void writes(char path[1000],char text[1000]){
    // Open file
    FILE* ptr = fopen(path,"a");
    // print teks
    fprintf(ptr,"%s\n",text);
    // Close file
    fclose(ptr);
}

// fungsi untuk menyimpan log dengan timestamp
void save_log(char *login_user, char *command){
    // inisialisasi
    char fpath[1000]={0};
    // path ke file dblog.log
    sprintf(fpath,"%s/dblog.log",databasePath); // --> databasePath/dblog.log

    // open file dengan command "a" untuk melakukan writes di akhir file
    FILE *file_out = fopen(fpath,"a");
    // inisialisasi waktu saat ini
    char time_now[100] = {0};
	time_t t = time(NULL);
    // pisahkan komponen waktu menggunakan fungsi struct
	struct tm *tm = localtime(&t);
    // format timestamp : "%Y-%m-%d %H:%M:%S"
	strftime(time_now,100,"%Y-%m-%d %H:%M:%S",tm); 
    // print log yang berisi timestamp, user, dan command yang dilakukan user
    fprintf(file_out, "%s:%s:%s\n",time_now,login_user,command); // --> 2023-06-18 15:26:10:root:CREATE USER sarah IDENTIFIED BY sarah123;
	
    // close file
    fclose(file_out);
}

// fungsi untuk menghapus spasi
char* trim(char *text){
    // inisialisasi
    int index = 0;
    // lakukan perulangan
    while(text[index] == '\t' || text[index] == ' ') index++;
    
    char *temp = strchr(text,text[index]);

    return temp;
}

// Fungsi untuk create user
int create_user(char *buffer,char *type){
    // Inisialisasi
    char tempBuffer[1024] = {0}; 
    // copy value buffer ke dalam tempBuffer
    strcpy(tempBuffer,buffer);
   
    // Memisahkan token pada tempBuffer yang dipisahkan oleh ";"
    char* token = strtok(tempBuffer, ";");
    // token selanjutnya dipisahkan oleh " "
    token = strtok(tempBuffer," ");

    // inisialisasi
    int i=0;
    char input[10][1000];
    
    // Cek jika user bukan root
    if(strcmp(type,"root")){ 
        printf("ERROR: Permission denied\n");
        return 0; // return 0 karena hanya root yang dapat melakukan command CREATE USER
    }

    // lakukan perulangan selama isi token tidak null
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    // Pengecekkan jumlah argumen
    if(i < 6){
        printf("ERROR: syntax error\n");
        return -1;
    }
    // Pengecekkan syntax IDENTIFIED CREATE USER [nama_user] IDENTIFIED BY [password_user];
    if(strcmp("IDENTIFIED",input[3]) || strcmp("BY",input[4]) || buffer[strlen(buffer)-1] != ';'){
        printf("ERROR: syntax error\n");
        return -1;
    }

    // Periksa user
    if(strcmp(input[2],"root") != 0 && !check_user(input[2])){ // Jika nama user tidak ada
        // inisialisasi
        char tmp[1000];
        // copy input nama ke tmp
        strcpy(tmp,input[2]);

        // tambahkan spasi
        strcat(tmp," ");

        // masukkan password
        strcat(tmp,input[5]);

        // tulis tmp pada userPath menggunakan fungsi file
        writes(userPath,tmp);
        return 1;
    }else {
        // syarat tidak memenuhi
        printf("ERROR: create user failed\n");
        return -2;
    }
}

// USE DATABASE
int use(char *buffer, char *type, char *login_user, char *useDatabase){
    // inisialisasi
    char tempBuffer[1024] = {0};
    char database[100] = {0};

    // copy buffer ke tempBuffer
    strcpy(tempBuffer,buffer);
    
    // pisahkan token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    
    // Jika token tidak null
    if(token != NULL){
        // copy token ke database
        strcpy(database,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    // open file dengan command "r" untuk read
    FILE *file_in = fopen(permissionPath,"r");

    // inisialisasi
    bool ada = false;
    char databasetmp[1000];
    char usertmp[1000];
    
    // Jika file ada
    if(file_in){
        // baca isi file sampai end of file
        while(fscanf(file_in,"%s %s",databasetmp,usertmp) != EOF){
            // cek jika database tersedia
            if(!strcmp(databasetmp,database)){
                ada = true;
                if(!strncmp(type,"root",4) || !strcmp(usertmp,login_user)){
                    // tutup file
                    fclose(file_in);
                    // copy database ke useDatabase
                    strcpy(useDatabase, database);
                    return 1;
                }
            }
        }
    }
    // Jika file tidak ada
    if(ada == false){
        printf("ERROR: syntax error\n");
        return -2;
    }
    return 0;
}

// CREATE DATABASE
int create_database(char *buffer, char *type, char *login_user){
    // inisialisasi
    char tempBuffer[1024] = {0};
    char data_base[100] = {0};
    // copy buffer ke tempBuffer
    strcpy(tempBuffer,buffer);
    
    // pisahkan token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    // jika token tidak null
    if(token != NULL){
        // copy token ke database
        strcpy(data_base,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    // inisialisasi path
    char fpath[1000] = {0};
    sprintf(fpath,"%s/databases/%s",databasePath,data_base); // --> databasePath/databases/database
    
    // buat direktori
    if(mkdir(fpath,0777) == 0){
        char record[1000] = {0};
        // print database login_user ke record
        sprintf(record,"%s %s", data_base, login_user);
        // tulis record ke dalam permissionPath menggunakan fungsi file
        writes(permissionPath,record);      
        return 1;
    }else{
        return 0;
    }
}

// Fungsi untuk mengecek permission
int grant_pemission(char *buffer, char *type){
    // inisialisasi
    int i=0;
    char tempBuffer[1024] = {0};
    // copy buffer ke dalam tempBuffer
    strcpy(tempBuffer,buffer);
   
    // pisahkan token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    char input[10][1000];
    
    // Cek jika user bukan root
    if(strcmp(type,"root")){
        printf("ERROR: Permission denied\n");
        return 0;
    }

    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    // pengecekan jumlah argumen
    if(i < 5){
        printf("ERROR: syntax error\n");
        return -1;
    }
    
    // pengecekan syntax
    if(buffer[strlen(buffer)-1] != ';' || strcmp("INTO",input[3])){
        printf("ERROR: Syntax error\n");
        return -1;
    }

    // inisialisasi
    char tmp[1000] = {0};
    // cek database
    if(check_database(input[2])){
        // cek user
        if(check_user(input[4])){
            // simpan data user dan database ke dalam variabel tmp
            sprintf(tmp, "%s %s", input[2], input[4]);
            // tulis permission untuk user dan database yang dipilih menggunakan fungsi writes
            writes(permissionPath,tmp);
            return 1;
        }else{
            // printf("ERROR: Permission failed\n");
            return -3;
        }
    }else{
        // printf("ERROR: Permission failed\n");
        return -2;
    }
}

// Fungsi untuk menjalankan command create table
int create_table(char *buffer,char *useDatabase){
    // cek apakah database ada
    if(strlen(useDatabase) == 0){
        printf("ERROR: database not found\n");
        return -2;
    }

    // inisialisasi
    char tempBuffer[1024] = {0};
    char table[100] = {0};
    
    strcpy(tempBuffer,buffer);// copy buffer ke tempBuffer
    
    // pisahkan token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    // Jika token tidak null
    if(token != NULL){
        strcpy(table,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    char *temp;
    temp = strchr(buffer,'(');
    if(!temp)return -1;
    else temp = temp + 1;
    
    // inisialisasi kolom
    char kolom[1000] = {0};
    strcpy(kolom,temp);

    // token 1
    char *token1 = strtok(kolom,";");
    token1 = strtok(kolom,")");
    int ind = 0;
    char split_kolom[100][100] = {0};
    token1 = strtok(kolom,",");
    // lakukan perulangan ketika token tidak null
    while(token1!=NULL){
        strcpy(split_kolom[ind++],trim(token1)); //copy
        token1 = strtok(NULL,",");
    }

    //Pengecekan nama dan tipe kolom
    for(int i = 0; i < ind; i++){
        // inisialisasi
        char temp2[1000] = {0};
        char nama_kolom[100] = {0};
        char type_column[20] = {0};

        strcpy(temp2,split_kolom[i]); //copy kolom ke temp
        char *token2 = strtok(temp2," ");

        strcpy(nama_kolom,token2);
        token2 = strtok(NULL," ");
        if(token2){
            strcpy(type_column,token2);
            // Cek apakah tipe data kolom sesuai
            if(strcmp(type_column,"int") && strcmp(type_column,"string")){ // jika tidak
                // Print error
                // printf("ERROR: syntax error\n");
                return -4;
            }
        }else{
            // printf("ERROR: syntax error\n");
            return -3;
        }
    }

    char fpath[1000] = {0};
    // path
    sprintf(fpath,"%s/databases/%s/%s.txt",databasePath,useDatabase,table); // -->databasePath/databases/useDatabase/table
    
    // open file
    FILE *open;
    if(!(open = fopen(fpath,"r"))){
        // open dengan mode "write"
        open = fopen(fpath,"w");
        // inisialisasi
        char struktur_table[1000] = {0};
        // path
        sprintf(struktur_table,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,table);

        // lakukan perulangan
        for(int i=0; i<ind; i++){
            writes(struktur_table,split_kolom[i]); // tulis ke dalam file dengan fungsi write yang telah dibuat
        }
        return 1;
    }else{
        printf("ERROR: syntax error\n");
        return 0;
    }
}

// Fungsi untuk menghapus folder
void *delete_directory(void *arg){
    // inisialisasi fork
    pid_t child = fork();
    if(child < 0){
        exit(EXIT_FAILURE);
    }
    // child process
    else if(child == 0){
        char *fpath = (char *) arg;
        // gunakan command rm untuk meremove directory
        char *argv[] = {"rm","-rf",fpath, NULL}; //gunakan -rf untuk menghapus directory beserta file di dalamnya
        execv("/bin/rm",argv); // eksekusi argv
    }
}

// Fungsi untuk drop database
int drop_database(char *buffer, char *type, char *login_user, char *useDatabase){
    // inisialisasi
    char tempBuffer[1024] = {0};
    char data_base[100] = {0};
    // copy buffer ke tempBuffer
    strcpy(tempBuffer,buffer);
    
    // pisahkan token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");
    
    if(token != NULL){
        strcpy(data_base,token);
    }else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    // buat file
    FILE *file_in, *file_out;

    // open file_in dengan path permissionPath
    file_in = fopen(permissionPath,"r");

    // inisialisasi
    char databasetmp[1000];
    char usertmp[1000];
    bool bisa = false;
    bool ada = false;

    // Jika file_in tersedia
    if(file_in){
        // baca file sampai end of file
        while(fscanf(file_in,"%s %s",databasetmp,usertmp) != EOF){
            // cek apakah database tersedia
            if(!strcmp(databasetmp,data_base)){
                ada = true;
                // cek apakah user memiliki izin terhadap database
                if(!strcmp(usertmp,login_user)){
                    // close file
                    fclose(file_in);
                    bisa = true;
                    break;
                }
            }
        }

        if((ada && !strcmp(login_user,"root")) || bisa){
            // inisialisasi path
            char fpath[1000] = {0};

            // print path
            sprintf(fpath,"%s/databases/%s",databasePath,data_base); // --> databasePath/databases/data_base

            // inisialisasi thread
            pthread_t thread1;
            int iret1 = pthread_create(&thread1,NULL,delete_directory,fpath);;
            pthread_join(thread1,NULL);
            
            // open file
            file_in = fopen(permissionPath,"r");
            char temp_permission[1000] = {0};

            // path ke file temp.txt
            sprintf(temp_permission,"%s/databases/administrator/temp.txt",databasePath); // --> databasePath/databases/administrator/temp.txt
            
            // open file temp_permission yaitu file temp.txt
            file_out = fopen(temp_permission,"w");
            // baca isi file sampai end of file
            while(fscanf(file_in,"%s %s",databasetmp,usertmp) != EOF){
                if(strcmp(databasetmp,data_base)){
                    char record[1000] = {0};
                    fprintf(file_out, "%s %s\n", databasetmp, usertmp);
                }
            }

            // close file temp.txt
            fclose(file_out);

            // remove
            remove(permissionPath);
            rename(temp_permission,permissionPath);

            // reset useDatabase
            if(!strcmp(useDatabase,data_base)){
                bzero(useDatabase,sizeof(useDatabase));
            }
            return 1;
        }        
    }
    if(!ada){
        printf("ERROR: Database not found\n");
        return -2;
    }
    return 0;
}

// Fungsi validasi
int validasi(char *buffer){

    // inisialisasi
    char tmp[1000];
    strcpy(tmp,buffer);
    
    // pengecekan tanda petik
    char g = '\'';
    if(tmp[0]== g && tmp[strlen(tmp)-1] == g){
        return 1;
    }

    // pengecekan angka
    for(int i=0; i<strlen(tmp) ;i++){
        if(tmp[i] < '0' || tmp[i] > '9'){
            return 0;
        }
    }

    return 2;
}

// Fungsi untuk menjalankan command INSERT
int insert(char *buffer, char *useDatabase){
    // jika database tidak ada
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }    

    // inisialisasi
    char tmp[1000];
    strcpy(tmp,buffer);

    // pisahkan token
    char *token = strtok(tmp,"(");
    token = strtok(NULL,"(");
    token = strtok(token,")");
    token = strtok(token,",");

    // inisialisasi
    int i = 0 , k = 0;
    char data[100][1000];

    // ketika token tidak null
    while(token!=NULL){
        strcpy(data[i],trim(token)); i++;
        token = strtok(NULL,",");
    }

    strcpy(tmp,buffer);
    token = strtok(tmp," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    // inisialisasi
    FILE *file_in,*file_out;
    char open[1000] = {0};
    char append[1000] = {0};

    // path
    sprintf(open,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,token);

    // open file open
    file_in = fopen(open,"r");
    // inisialisasi
    char data_type[100][1000];
    char tmp_type[1000],ret[1000];

    // Jika file ada
    if(file_in){
        // baca isi file sampai end of file
        while(fscanf(file_in,"%s %s",ret,tmp_type) != EOF){
            // copy data
            strcpy(data_type[k],tmp_type);
            k++;
        }
    }
    else{
        // printf("ERROR: File not found\n");
        return -2;
    }

    // path
    sprintf(append,"%s/databases/%s/%s.txt",databasePath,useDatabase,token);

    // open file append
    file_out = fopen(append,"a");
    if(k != i){
        // tutup file open
        fclose(file_in);
        // tutup file append
        fclose(file_out);
        return -3;
    }

    // validasi tipe data
    for(int j=0;j<i;j++){
        int val = validasi(data[j]);
        if(val == 1 && !strcmp(data_type[j],"string")) ; // jika tipe data string
        else if (val == 2 && !strcmp(data_type[j],"int")) ; // jika tipe data int
        else {
            // tutup file open
            fclose(file_in);
            // tutup file append
            fclose(file_out);
            printf("ERROR: syntax error\n");
            return -4;
        }  
    }

    // print data ke file
    for(int j=0;j<i-1;j++){
        fprintf(file_out,"%s,",data[j]);
    }
    fprintf(file_out,"%s\n",data[i-1]);
    // tutup file open
    fclose(file_in);
    // tutup file append
    fclose(file_out);

    return 1;
}

// Fungsi untuk command drop table
int drop_table(char *buffer, char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -2;
    }

    // inisialisasi
    char tmp[1000];
    char *token;
    // pisah token
    strcpy(tmp,buffer);
    token = strtok(tmp,";"); // pemisah tanda ";"
    token = strtok(token," "); // pemisah " "
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    char open[1000] = {0};
    // path
    sprintf(open,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,token);

    // buka file open
    FILE *file_in = fopen(open,"r");

    char append[1000]={0};
    // path
    sprintf(append,"%s/databases/%s/%s.txt",databasePath,useDatabase,token);

    // Jika file open ada
    if(file_in){
        // tutup file open
        fclose(file_in);

        // remove file
        remove(open);
        remove(append);
        return 1;
    }
    else{
        printf("ERROR: syntax error\n");
        return -1;
    }
}

// Fungsi untuk command drop column
int drop_column(char *buffer, char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -2;
    }

    // inisialisasi
    int i = 0,j = 0,k = 0;
    char tmp[10000];
    char input[10][1000];
    
    strcpy(tmp,buffer);
    // pisahkan token
    char *token;
    token = strtok(tmp,";"); // pemisah tanda ";"
    token = strtok(token," "); // pemisah " "
    // perulangan selama token tidak null
    while (token != NULL) {
        strcpy(input[k++],token);
        token = strtok(NULL, " ");
    }

    if(k != 5){
        printf("ERROR: syntax error\n");
        return -3;
    }

    // inisialisasi
    FILE *strukturIn,*strukturOut,*tableIn,*tableOut;
    
    // inisialisasi
    char open[1000]={0};
    char append[1000]={0};
    char a[1000]={0};
    char b[1000]={0};

    // path
    sprintf(open,"%s/databases/%s/struktur_%s",databasePath,useDatabase,input[4]);
    strcpy(a,open); //copy
    strcat(a,"2.txt");
    strcat(open,".txt");

    // open file open
    strukturIn = fopen(open,"r");
   
    char data_type[1000];
    char name[1000];

    // selama file open ada
    if(strukturIn){
        strukturOut = fopen(a,"w");
        // baca isi file sampai end of file
        while(fscanf(strukturIn,"%s %s",name,data_type) != EOF){
            // compare input[2] dengan name
            if(strcmp(input[2],name)){
                fprintf(strukturOut,"%s %s\n",name,data_type);
            }
            else i = j;
            j++;
        }
        // tutup file open
        fclose(strukturIn);
        // tutup file append
        fclose(strukturOut);
    }
    else{
        printf("ERROR: syntax error\n");
        return -1;
    }

    // path append
    sprintf(append,"%s/databases/%s/%s",databasePath,useDatabase,input[4]);
    strcpy(b,append); // copy append ke b
    strcat(b,"2.txt"); // print 2.txt di akhri file b
    strcat(append,".txt"); // print txt di akhri file b

    // open file append
    tableIn = fopen(append,"r");

    // oppen file b
    tableOut = fopen(b,"w");

    char ambil[1000];
    while(fgets(ambil,1000,tableIn)){
        // inisialisasi
        token = strtok(ambil,","); // pemisah ","
        int j = 0;
        char baru[1000];

        // copy "" ke baru
        strcpy(baru,"");

        // perulangan selama token tidak null
        while(token!=NULL){
            if(j!=i){
                strcat(baru,token); // print token di akhir file baru
                strcat(baru,","); // print "," di akhir file baru
            }
            token = strtok(NULL,",");
            j++;
        }
        baru[strlen(baru)-1] = 0;
        // print baru ke file b
        fprintf(tableOut,"%s",baru);
        if(baru[strlen(baru)-1] != '\n'){
            fprintf(tableOut,"\n");
        }
    }
    // close file open
    fclose(tableIn);
    // close file b
    fclose(tableOut);

    // remove file
    remove(open); rename(a,open);
    remove(append); rename(b,append);
    return 1;
}

// fungsi login untuk autentifikasi
int login(char* type, char *login_user){
    // Open file userPath
    FILE *file_in = fopen(userPath,"r");
    printf("%s\n",type);

    // inisialisasi
    char user[1000],pass[1000];
    char usertmp[1000],passtmp[1000];
    // pisah token
    char* token = strtok(type, " ");
    strcpy(user,token);
    token = strtok(NULL, " ");
    strcpy(pass,token);
    
    // selama file tidak kosong
    if(file_in){
        // baca isi file sampai end of file
        while(fscanf(file_in,"%s %s",usertmp,passtmp) != EOF){
            // pengecekan permission dan user
            if(!strcmp(passtmp,pass) && !strcmp(usertmp,user)){
                // tutup file
                fclose(file_in);
                strcpy(login_user,user);
                return 1;
            }
        }
        // tutup file
        fclose(file_in);
    }
    return 0; 
}

// Fungsi untuk command delete
int delete_from(char *buffer,char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }

    // inisialisasi buffer
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
    
    // pisah token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");

    // inisialisasi
    int i=0;
    char input[10][1000];

    // perulangan ketika token tidak null
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    if(i == 3){
        // path
        char path[10000];
        sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[2]);
        FILE *file_in,*file_out;

        // open file pada path
        file_in = fopen(path,"r");
        if(file_in){
            // tutup file
            fclose(file_in);

            // open file path
            file_out = fopen(path,"w");
            fprintf(file_out,"");

            // tutup file path
            fclose(file_out);
        }
        else{
            printf("ERROR: file not found\n");
            return -3;
        }
        return 1;
    }
    else if(i == 5){
        // Command Where
        if(strcmp(input[3],"WHERE")){
            return -2;
        }

        // inisialisasi
        char table_name[1000],table[1000];
        char path[10000],n_path[10000],struk[10000];
        char ret[1000], cmp[1000];
        int i = -1, k = 0;

        // pisah token
        token = strtok(input[4],"="); // tanda pemisah "="
        strcpy(table_name,token);

        token = strtok(NULL,"="); // tanda pemisah "="
        strcpy(cmp,token);

        sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[2]);

        // open file struk
        FILE *strukturIn = fopen(struk,"r");
        // Jika file ada
        if(strukturIn){
            // baca isi file sampai end of file
            while(fscanf(strukturIn,"%s %s",table,ret) != EOF){
                if(!strcmp(table_name,table)) i = k;
                k++;
            }
            if(i == -1){
                // tutup file
                fclose(strukturIn);
                return -4;
            }
            // tutup file
            fclose(strukturIn);
        }
        else{
            printf("ERROR: file not found\n");
            return -3;
        }

        // path
        sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[2]);
        sprintf(n_path,"%s/databases/%s/%s2.txt",databasePath,useDatabase,input[2]);

        // open file path
        FILE *file_in = fopen(path,"r");
        // open file n_path
        FILE *file_out = fopen(n_path,"w");
        char ambil[1000];

        // jika file path tersedia
        if(file_in){
            // ambil isi file
            while(fgets(ambil,1000,file_in)){
                token = strtok(ambil,","); // pemisah ","
                // inisialisasi
                int j = 0, flag = 0; 
                char baru[1000];

                strcpy(baru,"");
                // perulangan selama token tidak null
                while(token!=NULL){
                    strcat(baru,token); // print token di akhir file baru
                    strcat(baru,","); // print "," di akhir file baru
                    if(j==i){
                        // compare token dan cmp
                        if(!strcmp(cmp,token)) flag = 1;
                    }
                    token = strtok(NULL,",");
                    j++;
                }
                if(!flag){
                    baru[strlen(baru)-1] = 0;
                    // print baru ke dalam file
                    fprintf(file_out,"%s",baru);
                    if(baru[strlen(baru)-1] != '\n') fprintf(file_out,"\n");
                }
            }
            // tutup file path
            fclose(file_in);
            // tutup file n_path
            fclose(file_out);

            // remove file
            remove(path);
            rename(n_path,path);
            return 1;
        }
        else{
            printf("ERROR: file not found\n");
            return -3;
        }
    }
    else{
        return -2;
    }
}

// Fungsi untuk command update
int update(char *buffer,char *useDatabase){
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }

    // inisialisasi buffer
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
    
    // pisah token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");

    // inisialisasi
    int i=0 , k=0;
    char input[10][1000], table_name[1000], cmp[1000];
    char path[10000], n_path[10000], struk[10000];
    char ret[1000], table[1000], banding[1000],temp[1000],cmpwhere[1000];

    // perulangan ketika token tidak null
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    // Cek jika iput[2] bukan SET
    if(strcmp(input[2],"SET")){
        printf("ERROR: syntax error\n");
        return -2;
    }

    token = strtok(input[3],"="); // ambil token
    strcpy(table_name,token); // copy token
    token = strtok(NULL,"="); // ambil token
    strcpy(cmp,token); // copy token

    sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[1]);

    // open file struk
    FILE *strukturIn = fopen(struk,"r");
    i = -1;
    // jika file ada
    if(strukturIn){
        // baca file sampai end of file
        while(fscanf(strukturIn,"%s %s",table,ret) != EOF){
            if(!strcmp(table_name,table))i = k;
            k++;
        }
        if(i == -1){
            // tutup file
            fclose(strukturIn);
            return -4;
        }
        // tutup file
        fclose(strukturIn);
    }
    else{
        printf("ERROR: file not found\n");
        return -3;
    }

    int w = -1;
    if(strlen(input[4])){
        token = strtok(input[5],"="); // ambil token
        strcpy(banding,token); // copy token
        token = strtok(NULL,"="); // ambil token
        strcpy(cmpwhere,token); // copy token
        k=0;
        // commpare input dengan command WHERE
        if(!strcmp(input[4],"WHERE")){
            // open file struk
            strukturIn = fopen(struk,"r");
            // baca isi file sampai end of file
            while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                if(!strcmp(banding,temp)) {w = k;}
                k++;
            }
            if(w == -1){
                // tutup file
                fclose(strukturIn);
                return -4;
            }
            // tutup file
            fclose(strukturIn);
        }
    }

    // path file
    sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[1]);
    sprintf(n_path,"%s/databases/%s/%s2.txt",databasePath,useDatabase,input[1]);
    
    // open file path
    FILE *file_in = fopen(path,"r");
    // open file n_path
    FILE *file_out = fopen(n_path,"w");
    char ambil[1000];
    char lama[1000];
    // jika file ada
    if(file_in){
        while(fgets(ambil,1000,file_in)){
            strcpy(lama,ambil); // copy
            token = strtok(ambil,",");
            // inisialisasi
            int j = 0, flag = 0;
            char baru[1000];
            strcpy(baru,""); // copy
            
            // perulangan selama token tidak null
            while(token!=NULL){
                if(j == w){
                    if(!strcmp(cmpwhere,token))flag = 1;
                }
                if(j==i){
                    strcat(baru,cmp); // print cmp di akhir baru
                    strcat(baru,","); // print ","di akhir baru
                }
                else{
                    strcat(baru,token); // print token di akhir baru
                    strcat(baru,","); // print ","di akhir baru
                }
                token = strtok(NULL,",");
                j++;
            }
            if(w == -1 || flag == 1){
                baru[strlen(baru)-1] = 0;
                // print baru ke dalam file 
                fprintf(file_out,"%s",baru);
                if(baru[strlen(baru)-1] != '\n'){fprintf(file_out,"\n");}
            }
            else{
                lama[strlen(baru)-1] = 0;
                // print lama ke dalam file
                fprintf(file_out,"%s",lama);
                if(lama[strlen(lama)-1] != '\n'){fprintf(file_out,"\n");}
            }
        }
        // tutup file path
        fclose(file_in);
        // tutup file n_path
        fclose(file_out);

        // remove file
        remove(path);
        rename(n_path,path);
        return 1;
    } else{
        printf("ERROR: file not found\n");
    }
}

// Fungsi untuk command select
int select_table(char *buffer, char *useDatabase, int *newSocket){
    // cek database
    if(!strlen(useDatabase)){
        printf("ERROR: database not found\n");
        return -1;
    }

    // inisialisasi buffer
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
   
    // pisahkan token
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");

    // inisialisasi
    int i=0, w = -1, urut[20], all, k, kasus;
    char input[10][1000],banding[1000],temp[1000],cmpwhere[1000],ret[1000];
    char path[10000],n_path[10000],struk[10000],cmp[20][1000];

    // perulangan selama token tidak kosong
    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    FILE *strukturIn;
    if(!strcmp(input[1],"*")){
        // path
        sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[3]);

        // open file
        strukturIn = fopen(struk,"r");

        if(!strukturIn)return -3;
        // tutup file
        fclose(strukturIn);

        all = -1;
        kasus = 2;
        // compare input[2] dengan from
        if(strcmp(input[2],"FROM"))return -2;
        // compare input[5] dengan where
        if(i >= 5 && !strcmp(input[4],"WHERE")){
            token = strtok(input[5],"="); // ambil token
            strcpy(banding,token); // copy token
            token = strtok(NULL,"="); // ambil token
            strcpy(cmpwhere,token); // copy token
            k=0;
            // compare input[4] dengan where
            if(!strcmp(input[4],"WHERE")){
                // open file
                strukturIn = fopen(struk,"r");
                // baca file sampai end of file
                while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                    if(!strcmp(banding,temp)){w = k;}
                    k++;
                }
                if(w == -1){
                    // tutup file
                    fclose(strukturIn);
                    return -4;
                }
                // tutup file
                fclose(strukturIn);
            }
        }
    }
    else{
        kasus = 2;
        while(kasus <= i && strcmp(input[kasus++],"FROM")){ }
        
        if(kasus > i)return -2;
        
        sprintf(struk,"%s/databases/%s/struktur_%s.txt",databasePath,useDatabase,input[kasus]);
        // open file
        strukturIn = fopen(struk,"r");
        // jika file ada
        if(!strukturIn){
            printf("ka %d %s\n",kasus,struk);
            return -3;
        }

        // tutup file
        fclose(strukturIn);
        all = 0;kasus = 1;
        
        for(int s=0; s<20; s++){
            urut[s] = -1;
        }

        char temp2[1000];
        while(strcmp(input[kasus],"FROM") && kasus < i){
            k=0;
            // open file
            strukturIn = fopen(struk,"r");
            // baca file sampai eof
            while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                strcpy(temp2,temp); // copy
                strcat(temp2,",");
                if(!strcmp(temp2,input[kasus]) || !strcmp(input[kasus],temp)){
                    strcpy(cmp[all],input[kasus]); // copy input
                    urut[all] = k; all++;
                }
                k++;
            }
            if(urut[all-1] == -1 || all == 0){
                // tutup file
                fclose(strukturIn);
                return -4;
            }
            // tutup file
            fclose(strukturIn);
            kasus++;
        }
        if(kasus == i){
            return -3;
        }
        if(!strcmp(input[kasus+2],"WHERE") && (i >= kasus+2)){
            token = strtok(input[kasus+3],"="); // ambil token
            strcpy(banding,token); // copy token
            token = strtok(NULL,"="); // ambil token
            strcpy(cmpwhere,token); // copy token
            k=0;

            if(!strcmp(input[kasus+2],"WHERE")){
                // open file
                strukturIn = fopen(struk,"r");
                // baca file sampai eof
                while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                    if(!strcmp(banding,temp))w = k;
                    k++;
                }
                if(w == -1){
                    // tutup file
                    fclose(strukturIn);
                    return -4;
                }
                // tutup file
                fclose(strukturIn);
            }
        }
    }

    // path
    sprintf(path,"%s/databases/%s/%s.txt",databasePath,useDatabase,input[kasus+1]);
    sprintf(n_path,"%s/databases/%s/%s2.txt",databasePath,useDatabase,input[kasus+1]);
    
    // open file pada path
    FILE *file_in = fopen(path,"r");
    // open file pada n_path
    FILE *file_out = fopen(n_path,"w");
    // inisialisasi
    char ambil[1000], lama[1000], baru[1000];

    // jika file ada
    if(file_in){
        // inisialisasi
        char init[100] = {0};
        char confirm[1024]={0};
        
        strcpy(init,"Start"); // copy
        // kirim
        send(*newSocket , init , strlen(init) , 0 );
        // menerima respon
        int readValue = recv( *newSocket , confirm, 1024, 0);
        while(fgets(ambil,1000,file_in)){
            if(all == -1){
                if(w == -1){
                    // inisialisasi
                    char text[1000] = {0},buffer2[1000] = {0};
                    strcpy(text,ambil); // copy
                    // kirim dengan socket
                    send(*newSocket , text , strlen(text) , 0 );
                    // menerima melalui socket
                    readValue = recv( *newSocket , buffer2, 1024, 0);
                }
                else{
                    // inisialisasi
                    char hade[1000];
                    int j = 0,flag = 0;
                    strcpy(hade,ambil); // copy

                    // ambil token
                    token = strtok(ambil,",");
                    
                    strcpy(baru,"");
                    // perulangan selama token tidak null
                    while(token!=NULL){
                        if(j == w){
                            if(!strcmp(cmpwhere,token)){
                                // inisialisasi
                                char text[1000] = {0};
                                char buffer2[1000] = {0};
                                strcpy(text,hade);
                                // kirim
                                send(*newSocket , text , strlen(text) , 0 );
                                // menerima respon
                                readValue = recv( *newSocket , buffer2, 1024, 0);
                                // printf("%s\n",hade);
                                break;
                            }
                        }
                        // ambil token baru
                        token = strtok(NULL,",");
                        j++;
                    }
                }
            }
            else{
                // inisialisasi
                char jadi[1000];
                char hade[1000];
                strcpy(jadi,""); //copy
                // loop
                for(int s = 0; s < all; s++){
                    strcpy(hade,ambil); // copy
                    token = strtok(hade,",");
                    int j = 0, flag = 0;

                    strcpy(baru,""); //copy
                    while(token!=NULL){
                        if(j == urut[s]){
                            strcat(jadi,token);
                            strcat(jadi,",");
                            break;
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                }
                // inisialisasi
                char text[1000] = {0};
                char buffer2[1000] = {0};
                jadi[strlen(jadi)-1]='\0';

                // copy jadi ke dalam text
                strcpy(text,jadi);
                // kirim
                send(*newSocket , text , strlen(text) , 0 ); 
                //  menerima respon
                readValue = recv( *newSocket , buffer2, 1024, 0);
            }
        }
        // inisialisasi
        char text[1000] = {0};
        char buffer2[1024] = {0};
        strcpy(text, "EXIT");
        // kirim
        send(*newSocket , text , strlen(text) , 0 );
        // menerima respon
        readValue = recv( *newSocket , buffer2, 1024, 0);
    }
    else{
        return -2;
    }
}

// fungsi run
void *run(void *arg){
    // inisialisasi
    bool dump_client = false;
    int readValue;
    int *newSocket = (int *) arg;
    char type[1024] = {0};
    char tmptype[1024] = {0};
    char login_user[100] = {0};
    char useDatabase[100]={0};
    readValue = recv( *newSocket , type, 1024, 0);
    
    strcpy(tmptype,type);
    // cek root
    if(!strncmp(type,"root",4)){
        strcpy(login_user,"root");
        char status_login[1000] = {0};
        strcpy(status_login,"Authentication success");
        send(*newSocket , status_login , strlen(status_login) , 0 );

        char *ret = strstr(tmptype,"dump");
        if(ret){
            dump_client = true;
        }
    }
    else{
        //cek login user
        int masuk = login(type,login_user);
        char status_login[1000] = {0};
        
        // jika gagal login
        if(masuk == 0){
            strcpy(status_login,"ERROR: Authentication failed");
            send(*newSocket , status_login , strlen(status_login) , 0 );
            printf("ERROR: Login failed\n");
            close(*newSocket);
            return;
        }
        strcpy(status_login,"Authentication success");
        send(*newSocket , status_login , strlen(status_login) , 0 );

        printf("Login success%s\n",login_user);

        char *ret = strstr(tmptype,"client_dump");
        if(ret){
            dump_client = true;
        }
    }

    if(dump_client){
        char buffer[1024] = {0};
        char message[1024] = {0};

        readValue = recv( *newSocket , buffer, 1024, 0);
        if(!strncmp(buffer,"USE",3)){
            save_log(login_user, buffer);
            int status = use(buffer,type,login_user,useDatabase);
            // jika database tidak ditemukan
            if(status == -2){
                strcpy(message,"ERROR: Unknown database");
                close(*newSocket);
            }else if(status == -1){ //syntax error
                strcpy(message,"ERROR: Syntax error");
                close(*newSocket);
            }else if(status == 1){
                sprintf(message,"ERROR: Using database: %s",useDatabase);
            }else if(status == 0){ //permission denied
                strcpy(message,"ERROR: Permission denied");
                close(*newSocket);
            }
            // kirim
            send(*newSocket , message , strlen(message) , 0 );
            // menerima respon
            readValue = recv( *newSocket , buffer, 1024, 0);
            //generate command
            generate_command(newSocket,useDatabase);
        }
        close(*newSocket);
        return 0;
    }
    while(1){
        char buffer[1024] = {0};
        char message[1024] = {0};
        // test
        char *hello = "Hello, this is from server!";

        readValue = recv( *newSocket , buffer, 1024, 0);
        printf("%s\n", buffer);

        // cek koneksi
        if(isConnectionClosed(readValue,newSocket)){
            printf("ERROR: Connection error\n");
            break;
        }

        // create user
        if(!strncmp(buffer,"CREATE USER",11)){
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = create_user(buffer,type);
            if(status == -2) strcpy(message,"ERROR: User already exist");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
            else if(status == 1) strcpy(message,"Create user success");
            
        }else if(!strncmp(buffer,"USE",3)){ // USE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = use(buffer,type,login_user,useDatabase);
            if(status == -2) strcpy(message,"ERROR: Unknown database");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) sprintf(message,"Using database: %s",useDatabase);
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
        
        }else if(!strncmp(buffer,"CREATE DATABASE",15)){ // CREATE DATABASE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = create_database(buffer,type,login_user);
            if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) strcpy(message,"Create success");
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
            
        }else if(!strncmp(buffer,"GRANT PERMISSION",16)){ // GRANT PERMISSION
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = grant_pemission(buffer,type);
            if(status == -3) strcpy(message,"ERROR: Unknown user");
            else if(status == -2) strcpy(message,"ERROR: Database not exist");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) strcpy(message,"Grant permission success");
            else if(status == 0) strcpy(message,"ERROR: Grant permission denied");
            
        }else if(!strncmp(buffer,"CREATE TABLE",12)){ // CREATE TABLE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = create_table(buffer,useDatabase);
            if(status == -4) strcpy(message,"ERROR: Invalid column type");
            else if(status == -3) strcpy(message,"ERROR: Missing column type");
            else if(status == -2) strcpy(message,"ERROR: No database used");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) strcpy(message,"Create success");
            else if(status == 0) strcpy(message,"ERROR: Table already exist");
            
        }else if(!strncmp(buffer,"DROP DATABASE",13)){ // DROP DATABASE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = drop_database(buffer,type,login_user,useDatabase);
            if(status == -2) strcpy(message,"ERROR: Unknown database");
            else if(status == -1) strcpy(message,"ERROR: Syntax error");
            else if(status == 1) sprintf(message,"Database dropped");
            else if(status == 0) strcpy(message,"ERROR: Permission denied");
            
        }else if(!strncmp(buffer,"INSERT INTO",11)){ // INSERT INTO
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = insert(buffer,useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"Insert success");
                    break;
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -3:
                    strcpy(message,"ERROR: Coloumn count doesnt match");
                    break;
                case -4:
                    strcpy(message,"ERROR: Invalid input");
                    break;
            }
        }else if(!strncmp(buffer,"DROP TABLE",10)){ // DROP TABLE
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = drop_table(buffer,useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"Drop table success");
                    break;
                case -1:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -2:
                    strcpy(message,"ERROR: No database used");
                    break;
            }
        }else if(!strncmp(buffer,"DROP COLUMN",11)){ // DROP COLUMN
            // simpan log sesuai timestamp
            save_log(login_user, buffer);
            int status = drop_column(buffer,useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"Drop column success");
                    break;
                case -1:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -2:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -3:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
            }
        }else if(!strncmp(buffer,"DELETE FROM",11)){ //DELETE FROM
            int status = delete_from(buffer,useDatabase);
            switch(status){
                case 1:
                    strcpy(message,"Delete success");
                    break;
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
                case -3:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -4:
                    strcpy(message,"ERROR: Column does not exist");
                    break;
            }
        }else if(!strncmp(buffer,"UPDATE",6)){
            int status = update(buffer,useDatabase);
            switch(status){
                case 1:
                    strcpy(message,"Update success");
                    break;
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
                case -3:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -4:
                    strcpy(message,"ERROR: Column does not exist");
                    break;
            }
        }else if(!strncmp(buffer,"SELECT",6)){ // SELECT
            int status = select_table(buffer,useDatabase,newSocket);
            switch(status){
                case -1:
                    strcpy(message,"ERROR: No database used");
                    break;
                case -2:
                    strcpy(message,"ERROR: Invalid syntax");
                    break;
                case -3:
                    strcpy(message,"ERROR: Table does not exist");
                    break;
                case -4:
                    strcpy(message,"ERROR: Column does not exist");
                    break;
                default:
                    strcpy(message,"Select success");
                    break;
            }
        }else if(!strncmp(buffer,"EXIT", 5)){
            printf("Socket closed\n");
            close(*newSocket);
        }else{
            save_log(login_user, buffer);
            strcpy(message,"ERROR: Syntax error");
        }
        //debug user dan database yg digunakan
        if(!strcmp(buffer,"STATUS")){ //STATUS
            sprintf(message,"login_user:%s useDatabase:%s",login_user,useDatabase);
        } 
        // kirim
        send(*newSocket , message , strlen(message) , 0 );
    }
}

// membuat fungsi daemon process agar program dapat berjalan secara daemon
void daemon_process(){
    // inisialisasi fork
    pid_t child = fork();

    // pengecekan pid
    // gagal
    if(child < 0) {
        exit(EXIT_FAILURE);
    }
    // sukses
    else if(child > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    // inisialisasi session
    pid_t childSID = setsid();

    // pengecekan sid
    // gagal
    if(childSID < 0) {
        exit(EXIT_FAILURE);
    }
    if((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int main(int argc, char const *argv[]) {
    // inisialisasi
    // dbpath
    char dbpath[1000] = {0};
    sprintf(dbpath,"%s/databases",databasePath);
    // adminpath
    char adminpath[1000] = {0};
    sprintf(adminpath,"%s/administrator",dbpath);

    // mkdir
    mkdir(dbpath,0777);
    mkdir(adminpath,0777);

    // inisialisasi socket
    int server_fd, newSocket[MAX_USER];
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("ERROR: Socket failed");
        exit(EXIT_FAILURE);
    }

    int reuseaddr = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)) < 0) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    } 

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    // daemon process
    daemon_process();

    int ctr = 0;
    while(1){
        if ((newSocket[ctr] = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("Accept\n");
            exit(EXIT_FAILURE);
        }
        // create thread
        pthread_create(&(tid[ctr]),NULL,run,&newSocket[ctr]);
        ctr++;
        printf("Client %d connected\n", ctr);
    }
    return 0;
}