#!/bin/bash

# Buat file sementara untuk menyimpan jadwal cron
cron_file=$(mktemp)

# Isi file sementara dengan jadwal cron yaitu akan dijalankan setiap 1 jam
echo "0 */1 * * * cd /home/sarahnrhsna/Documents/sisop/fp/database && /usr/bin/zip -rm `date \"+\%Y-\%m-\%d\"`\ `date +\"\%H:\%M:\%S\"` dblog.log *.backup" > $cron_file

# Impor jadwal cron dari file sementara
crontab $cron_file

# Hapus file sementara
rm $cron_file
