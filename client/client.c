// Impor Header yang akan digunakan
#include <arpa/inet.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>

// Definisi PORT
#define PORT 8080

// Deklarasi Fungsi yang digunakan 
bool isConnectionClosed(int readValue, int newSock);

// Main Program
int main(int argc, char const *argv[]){
	
	// Digunakan untuk menyimpan informasi alamat client
	struct sockaddr_in clientAddress;
	
	// Digunakan untuk menyimpan informasi alamat server
	struct sockaddr_in serverAddress;
	
	// Digunakan untuk berkomunikasi dengan client
	int newSock = 0;
	
	// Digunakan untuk menyimpan respons dari koneksi atau operasi lainnya
	int readValue;
	
	// Menunjukkan data tidak berasal dari Secondary Source
	bool isDataFromSecondarySource = false;
	
	// Inisialisasi array of char untuk variable type
	char type[1024];
	
	// Inisialisiasi array of char untuk variable statusLogin
	char statusLogin[1000] = {0};
	
	// Inisialisasi varibale untuk menyimpan jumlah byte yang diterima
	int receivedBytes;
	
	// Mengecek apakah newSock < 0
	if((newSock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		// Kondisi terpenuhi
		printf("\n ERROR: Socket creation failed \n");
		
		// Menunjukkan adanya kesalahan dalam pembuatan socket
		return -1;
	}
	
	
	// Digunakan untuk mengatur nilai awal serverAddress dengan nilai 0
	memset(&serverAddress, '0', sizeof(serverAddress));
	
	// Digunakan untuk menetapkan jenis alamat yang akan digunakan
	serverAddress.sin_family = AF_INET;
	
	// Digunakan untuk menentukan nomor port yang akan digunakan pada koneksi
	serverAddress.sin_port = htons(PORT);
	
	// Mengecek apakah hasil konversi kurang <= 0
	if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0){
		// Kondisi terpenuhi
		printf("\nERROR: Address not found\n");
		
		// Menandakan adanya kesalahan dalam konversi alamat IP
		return -1;
	}
	
	// Mengecek apakah koneksi ke server berhasil dilakukan
	if(connect(newSock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0){
		
		// Jika tidak bisa melakukan koneksi ke server
		printf("\nERROR: socket connection failed\n");
		
		// Menandakan bahwa koneksi gagal
		return -1;
	}
	
	
	// Mengecek apakah getuid tidak bernilai 0
	if(getuid()){
	
		// Memeriksa apakah jumlah argumen kurang dari 5
		if(argc < 5){
			// Kondisi terpenuhi
			printf("ERROR: too few arguments in function call\n");
			
			// Menandakan adanya kesalahan
			return -1;
		}
		
		/* Memeriksa apakah argumen pada indeks 1 adalah "-u" 
		   dan argumen pada indeks 3 adalah "=p"*/
		if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p")){
			
			// Jika tidak
			printf("ERROR: syntax error\n");
			
			// Menandakan adanya kesalahan
			return -1;
		}
		
		// Memeriksa apakah jumlah argumen = 7
		if(argc == 7){
		
			// Memeriksa apakah argumen pada indeks 5 adalah "-d" 
			if(!strcmp(argv[5], "-d")){
				// Jika ya
				isDataFromSecondarySource = true;
			} 
			// Jika tidak
			else {
				// Mencetak pesan
				printf("ERROR: syntax error\n");
				
				// Menandakan adanya kesalahan
				return -1;
			}
		}
	} 
	// Jika getUID bernilai 0 / tidakberhasil mendapatkan UID
	else {
		// Memeriksa apakah jumlah argumenm = 3
		if(argc == 3){
		
			// Jika iya, maka memeriksa apakah argumen pada indeks 1 adalah "-d"
			if(!strcmp(argv[1],"-d")){
				// Jika kondisi terpenuhi
				isDataFromSecondarySource = true;
			} 
			// Jika salah
			else {
				// Mencetak pesan
				printf("ERROR: syntax error\n");
				
				// Menandakan adanya kesalahan
				return -1;
			}
		}
	}
	
	// Memeriksa apakah getUID() mengembalikan nilai bukan 0
	if(getuid()){
	
		// Menyalin string dari argv[2], ke type
		strcpy(type, argv[2]);
		
		// Menggabungkan spasi ke type
		strcat(type, " ");
		
		// Menggabungkan string dari argv[4] ke type
		strcat(type, argv[4]);
		
		// Mengirimkan data type melalui socket
		send(newSock, type, strlen(type), 0);
	} 
	// Jika getUID() mengembalikan nilai 0
	else {
		// Menyalin string root ke type
		strcpy(type, "root");
		
		// Mengirimkan data dalam type melalui socket
		send(newSock, type, strlen(type), 0);
	}
	
	// Mengembalikan jumlah byte yang berhasil diterima dan disimpan
	receivedBytes = recv(newSock, statusLogin, 1000, 0);
	
	// Mencetak statusLogin
	printf("%s\n", statusLogin);
	
	// Memeriksa apakah string dalam statusLogin berisi Authentication failed
	if(!strcmp(statusLogin, "ERROR: Authentication failed")){
		
		// Menutup socket milik kliend
		close(newSock);
		
		// Menandakan adanya kesalahan
		return -1;
	}
	
	// Memeriksa apakah getUID() mengembalikan nilai bukan 0
	if(getuid()){
		
		// Memeriksa apakah isDataFromSecondarySource bernilai true
		if(isDataFromSecondarySource){
		// Kondisi Terpenuhi
			
			// Inisialisasi variable cmd
			char cmd[1000] = {0};
			
			// Inisialisai variable receivedData
			char receivedData[1000] = {0};
			
			// Mengisi array cmd
			sprintf(cmd, "USE %s", argv[6]);
			
			// Mengirimkan data pada cmd melalui socket
			send(newSock, cmd, strlen(cmd), 0);
			
			// Menerima data yang dikirimkan lewat socket berupa jumlah byte
			receivedBytes = recv(newSock, receivedData, 1000, 0);
			
			// Memeriksa apakah string yang diterima = database changed to sepanjang 19 karakter
			if(!strncmp(receivedData, "Using database: ", 19)){
			// Kondisi terpenuhi
				
				// Inisialisasi variable temp
				char temp[1000];
				
				
				// Mengulangi proses membaca input dari stdin 
				while((fscanf(stdin, "%[^\n]%*c", temp)) != EOF){
					
					// Mengirimkan data pada temp melalui socket
					send(newSock, temp, strlen(temp), 0);
					
					// Menghapus isi array receivedData
					bzero(receivedData, sizeof(receivedData));
					
					// Menerima data yang dikirimnya melalui socket
					receivedBytes = recv(newSock, receivedData, 1000, 0);
					
					// Mencetak isi dari receivedData
					printf("%s\n", receivedData);
				};
				
				// Menutup socket
				close(newSock);
				
				// Mengembalikan nilai 0
				return 0;
			}
		}
	} 
	// Jika kondisi tidak terpenuhi
	else {
		// // Memeriksa apakah isDataFromSecondarySource bernilai true
		if(isDataFromSecondarySource) {
			// Inisialisasi variable cmd
			char cmd[1000] = {0};
			
			// Inisialisai variable receivedData
			char receivedData[1000] = {0};
			
			// Mengisi array cmd
			sprintf(cmd, "USE %s", argv[2]);
			
			// Mengirimkan data pada cmd melalui socket
			send(newSock, cmd, strlen(cmd), 0);
			
			// Menerima data yang dikirimkan lewat socket berupa jumlah byte
			receivedBytes = recv(newSock, receivedData, 1000, 0);
			
			// Memeriksa apakah string yang diterima = database changed to sepanjang 19 karakter
			if(!strncmp(receivedData, "Using database: ", 19)){
			// Kondisi terpenuhi
				
				// Inisialisasi variable temp	
				char temp[1000];
				
				// Mengulangi proses membaca input dari stdin 
				while((fscanf(stdin, "%[^\n]%*c", temp)) != EOF){
				
					// Mengirimkan data pada temp melalui socket
					send(newSock, temp, strlen(temp), 0);
					
					// Menghapus isi array receivedData
					bzero(receivedData, sizeof(receivedData));
					
					// Menerima data yang dikirimnya melalui socket
					receivedBytes = recv(newSock, receivedData, 1000, 0);
					
					// Mencetak isi dari receivedData
					printf("%s\n", receivedData);
				};
				
				// Menutup socket
				close(newSock);
				
				// Mengembalikan nilai 0
				return 0;
			}
		} 
	}
	
	// Membuat loop tak terbatas
	while(true){
	
		// Deklarasi variable message
		char message[1000] = {0};
		
		// Deklarasi variable buffer
		char buffer[1024] = {0};
		
		// Membaca input dari pengguna dan menyimpan dalam variable message
		gets(message);
		
		// Mengirimkan data pada message melalui socket
		send(newSock, message, strlen(message), 0);
		
		// Menerima data yang dikirim melalui socket
		readValue = recv(newSock, buffer, 1024, 0);
		
		
		// Mengecek apakah koneksi ditutup dengan memanggil fungsi isConnectionClosed
		if(isConnectionClosed(readValue, newSock)){
			// Jika kondisi terpenuhi
			break;
		}
		
		// Memeriksa apakah string dalam buffer = BEGIN
		if(!strcmp(buffer, "BEGIN")){
		// Kondisi terpenuhi
			
			// Deklarasi variable cmd
			char cmd[1000] = {0};
			
			// Deklarasi variable receivedData
			char receivedData[1000] = {0};
			
			// Menyalin string "ok" ke dalam cmd
			strcpy(cmd, "ok");
			
			// Mengirimkan data pada cmd melalui socket
			send(newSock, cmd, strlen(cmd), 0);
			
			// Memulai loop do-while untuk menerima dan memproses data sampai menerima string "SUCCES"
			do{
				// Mengisi array receivedData dengan 0
				bzero(receivedData, sizeof(receivedData));
				
				// menerima data dari newSock dan menyimpannya dalam receivedData
				receivedBytes = recv(newSock, receivedData, 1000, 0);
				
				// Memeriksa apakah string dalam receivedData 
				if(strcmp(receivedData, "SUCCESS")){
				// Kondisi terpenuhi
					
					// Memeriksa apakah karakter terakhir dalam receivedData bukan newline 
					if(receivedData[strlen(receivedData) -1] != '\n'){
						// Kondisi terpenuhi
						printf("%s\n", receivedData);
					}
					// Kondisi tidak terpenuhi
					else{
						// Mencetak isi receivedData
						printf("%s", receivedData);
					}
					
					// Mengisi array cmd dengan 0
					bzero(cmd, sizeof(cmd));
					
					// menyalin string "ok" ke cmd
					strcpy(cmd, "ok");
					
					// Mengirimkan data dalam array cmd melalui socket
					send(newSock, cmd, strlen(cmd), 0);
				}
			}
			// Kembali ke awal loop dan melanjutkan proses hinggal string receivedData adalah "SUCCESS"
			while(strcmp(receivedData, "SUCCESS"));
			
			// Mengisi array cmd dengan 0
			bzero(cmd, sizeof(cmd));
			
			// menyalin string "ok" ke cmd
			strcpy(cmd, "ok");
			
			// Mengirimkan data dalam array cmd melalui socket
			send(newSock, cmd, strlen(cmd), 0);
			
			// menerima data dari newSock dan menyimpannya dalam buffer
			receivedBytes = recv(newSock, buffer, 1024, 0);
			
			// mencetak isi dari array buffer
			printf("%s\n", buffer);
			
		} 
		// Jika kondisi tidak terpenuhi
		else {
			// Mencetak isi dari array buffer
			printf("%s\n", buffer);
		}
	}
	
	// Mengembalikan nilai 0
	return 0;
}


// Mengecek apakah koneksi koneksi sudah ditutup atau masih aktif
bool isConnectionClosed(int readValue, int newSock){
	
	// Jika readValue = 0, artinya koneksi ditutup
	if(!readValue){
		// Menutup socket
		close(newSock);
		
		// Mengembalikan nilai true untuk menandakan koneksi ditutup
		return true;
	}
	
	// Mengembalikan nilai false jika koneksi masih aktif
	return false;
}
